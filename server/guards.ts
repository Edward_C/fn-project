import { Bearer } from 'permit';
import jwtSimple from 'jwt-simple';
import express from 'express';
import jwt from './jwt';
import { UserServices } from './services/UserServices';
import { ActicityService } from './services/ActicityService';


const permit = new Bearer({
    query: "access_token"
})

export function createIsLoggedIn(userService: UserServices) {
    return async function (req: express.Request, res: express.Response, next: express.NextFunction) {
        try {
            const token = permit.check(req);
            if (!token) {
                return res.status(401).json({ msg: "Permission Denied1" });
            }
            const payload = jwtSimple.decode(token, jwt.jwtSecret);
            const user = await userService.getUserByUserID(payload.id);
            if (user) {
                const { password, firebase_token, ...others } = user;
                req.user = { ...others }
                return next();
            } else {
                return res.status(401).json({ msg: "Permission Denied2" });
            }
        } catch (e) {
            return res.status(401).json({ msg: "Permission Denied3" });
        }
    }
}

export function createIsInitiator(acticityService: ActicityService) {
    return async function (req: express.Request, res: express.Response, next: express.NextFunction) {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            const { users_id } = req.user;
            const { activityID } = req.body

            const isInitiator = await acticityService.getInitiatorByActivityID(users_id, activityID)

            if (isInitiator) {
                return next();
            }
            return res.status(403).json({ message: "Permission Denied(not Initiator)" });
        } catch (e) {
            return res.status(403).json({ message: "Permission Denied(not Initiator)" });
        }
    }
}