import express from 'express';
import { userRoutes } from './routes/userRoutes';
import { activityRoutes } from './routes/activityRoutes';
import { friendsRoutes } from './routes/friendsRoutes';

export const routes = express.Router();

routes.use('/user', userRoutes);
routes.use('/activity', activityRoutes);
routes.use('/friends', friendsRoutes);