import { FriendsService } from '../services/FriendsService';
import { Request, Response } from 'express';
import { logger } from '../logger';

export class FriendsController {
    constructor(private fiendsService: FriendsService) { }

    search = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            const { users_id } = req.user
            const name = req.body.searchBox
            const friendID = await this.fiendsService.getIDByName(name);
            const checkStatus = await this.fiendsService.getRequest(friendID[0].users_id,);
            if (checkStatus.length > 0) {
                res.status(403).json({ message: "Send request already" })
            } else if (users_id === friendID[0].users_id) {
                res.status(403).json({ message: "Can't send request to yourself" })
            } else {
                const result = await this.fiendsService.searchUser(friendID[0].users_id, users_id);
                res.status(200).json(result)
            }
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ message: 'FD_Controller - Search fail' });
        };
    }

    addFriends = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            const users_id = req.user.users_id;
            const friend = req.body.addFriendsName;
            const friendID = await this.fiendsService.getIDByName(friend);
            const friendShip = await this.fiendsService.addFriend(users_id, friendID[0]["users_id"]);
            res.status(200).json(friendShip);
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ message: 'FD_Controller - Add Friends fail' });
        }
    }

    getRequest = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            const { users_id } = req.user
            const getFriendsRequest = await this.fiendsService.getRequest(users_id)
            res.status(200).json(getFriendsRequest)
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ message: 'FD_Controller - Get Request fail' });
        }
    }

    acceptRequest = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            const { users_id } = req.user
            const user = req.body.requestUser;
            const acceptRequest = await this.fiendsService.acceptRequest(user, users_id);
            res.status(200).json(acceptRequest);
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ message: 'FD_Controller - Response fail' })
        }
    }

    rejectRequest = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            const { users_id } = req.user
            const user = req.body.requestUser;
            const rejectRequest = await this.fiendsService.rejectRequest(user, users_id);
            res.status(200).json(rejectRequest);
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ message: 'FD_Controller - Response fail' })
        }
    }

    friendsList = async (req: Request, res: Response) => {
        try {
            const userID = req.user?.users_id;
            const friendsList = await this.fiendsService.getFriendsList(userID);
            res.status(200).json(friendsList);
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ message: 'FD_Controller - Get fiends list fail' })
        }
    }
}