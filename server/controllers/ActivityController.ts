import { Request, Response } from 'express'
import { ActicityService } from "../services/ActicityService";
// import { UserServices } from '../services/UserServices'
import { logger } from '../logger';
// import { sendNotificationToClient } from '../firebase/notify';

interface IQuery {
    from: string
    to: string
}

interface IsendNotificationToClient {
    (tokens: string[], data: {
        [key: string]: string;
    }): void
}

export class ActivityController {
    constructor(
        private activityService: ActicityService,
        private sendNotificationToClient: IsendNotificationToClient
    ) { }

    getActivityByUesrID = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            if (!req.query || !req.query.from || !req.query.to) {
                res.status(400).json({ message: 'Please input the time' })
                return
            }
            // console.log(req.query)
            const { users_id } = req.user
            const { from, to } = req.query as any as IQuery
            const activities = await this.activityService.getActivityByUesrID(users_id, from, to)

            res.status(200).json(activities)
        } catch (e) {
            logger.error(e.message)
            res.status(500).json({ message: 'internal server error' });
        }
    }


    postActivity = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            const { users_id } = req.user
            // check time conflict
            // const getFreeTime = await this.activityService.getFreeTime(users_id, req.body)
            // console.log(getFreeTime)
            // if (getFreeTime) {
            //     res.status(403).json({ message: `time conflict` })
            //     return
            // }

            const id = await this.activityService.createActivity(users_id, req.body)
            res.status(200).json(id)

        } catch (e) {
            logger.error(e.message)
            res.status(500).json({ message: 'internal server error' });
        }
    }

    postAttend = async (req: Request, res: Response) => {
        try {
            const { friendUserID, activityID } = req.body
            const id = await this.activityService.inviteUser(friendUserID, activityID)
            if (!id) {
                res.status(400).json({ message: `User is already intivited.` })
                return
            }
            const { firebase_token } = await this.activityService.selectFirebaseToken(friendUserID)
            if (firebase_token) {
                const notificationData = {
                    title: 'Event assistance',
                    body: `${req.user?.name} is inviting you.`,
                    link: `/activities/${activityID}`
                };
                this.sendNotificationToClient(JSON.parse(firebase_token), notificationData);
            }

            res.status(200).json({ message: `inviting user is success` })
        } catch (e) {
            logger.error(e.message)
            res.status(500).json({ message: 'internal server error' });
        }
    }

    createPersonalActivityController = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            const { users_id } = req.user;
            // console.log(`ActivityControllers - ${users_id}`);
            // console.log(`ActivityControllers - ${JSON.stringify(req.body)}`);
            const createdAloneActivity = await this.activityService.createPersonalActivityServices(users_id, req.body)
            res.status(200).json({ message: createdAloneActivity });
        } catch (e) {
            logger.error(e.message)
            res.status(500).json({ message: 'internal server error' });
        }
    }

    getPersonalActivitiesByDateController = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            const { users_id } = req.user;
            // console.log(`ActivityControllers - getPersonalActivitiesByDateController : ${JSON.stringify(req.body)}`);
            const personalActivities = await this.activityService.getPersonalActivitiesByDateServices(users_id, req.body)
            // console.log(personalActivities);
            res.status(200).json(personalActivities)
        } catch (e) {
            logger.error(`getPersonalActivitiesByDateController: ${e.message}`)
            res.status(500).json({ message: 'Get personal activities by date fail' })
        }
    }

    deletePersonalActivitiesController = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const deletePersonalActivities = await this.activityService.deletePersonalActivitiesServices(req.body.activityID);
                res.status(200).json(deletePersonalActivities);
            }
        } catch (e) {
            logger.error(e.message)
            res.status(500).json({ message: 'Delete personal activities fail' })
        }
    }

    putAttend = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            const { users_id } = req.user;
            const { activityID, status, now } = req.body
            const isDeadline = await this.activityService.getDeadline(activityID, now)
            if (isDeadline) {
                res.status(403).json({ message: ` you've missed the deadline` })
                return
            }
            await this.activityService.putAttend(users_id, activityID, status)
            res.status(200).json({ message: ` changing status is success` })

        } catch (e) {
            logger.error(e.message)
            res.status(500).json({ message: 'internal server error' });
        }
    }

    getFriendActivitiesController = async (req: Request, res: Response) => {
        try {
            const getFriendActivitiesController = await this.activityService.getFriendActivitiesServices(req.body.friendsID, req.body.start_time, req.body.end_time);
            res.status(200).json(getFriendActivitiesController);
            // console.log(req.body)
            // console.log(JSON.stringify(getFriendActivitiesController));
        } catch (e) {
            logger.error(e.message);
            res.status(500).json({ message: 'getFriendActivitiesController error' })
        }
    }

    getFriendsattends = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            if (!parseInt(req.query.activityID as any, 10)) {
                res.status(400).json({ message: 'wrong query' })
                return
            }
            const { users_id } = req.user;
            const activityID = parseInt(req.query.activityID as string, 10)
            const friendsInvited = await this.activityService.getFriendsInvited(users_id, activityID)
            const friendsNotInvited = await this.activityService.getFriendsNotInvited(users_id, activityID)
            // console.log(friendsInvited)
            // console.log(friendsNotInvited)
            res.status(200).json({ friendsInvited, friendsNotInvited });
        } catch (e) {
            logger.error(e.message);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    setAvailableTimeController = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            const { users_id } = req.user;
            const setAvailableTime = await this.activityService.setAvailableTimeServices(users_id, req.body.start_time, req.body.end_time);
            res.status(200).json(setAvailableTime);
        } catch (e) {
            logger.error(e.message);
            res.status(500).json({ message: 'setAvailableTimeController error' })
        }
    }

    getAvailableTimeController = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            const { users_id } = req.user;
            const getAvailableTime = await this.activityService.getAvailableTimeServices(users_id, req.body.start_time, req.body.end_time);
            res.status(200).json(getAvailableTime);
        } catch (e) {
            logger.error(e.message);
            res.status(500).json({ message: 'getAvailableTimeController error' })
        }
    }

    getFriendAvailableTimeController = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            const { user_id, start_time, end_time } = req.body;
            const getFriendAvailableTime = await this.activityService.getAvailableTimeServices(user_id, start_time, end_time);
            res.status(200).json(getFriendAvailableTime);
        } catch (e) {
            logger.error(e.message);
            res.status(500).json({ message: 'getFriendAvailableTimeController error' })
        }
    }

    inviteFriendController = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            const { users_id } = req.user;
            const activityID = await this.activityService.createActivity(users_id, req.body)
            const activityIDNum = activityID[0] as number
            if (!activityIDNum) {
                return;
            } else {
                const inviteResponse = await this.activityService.inviteUser(req.body.friendID, activityIDNum);
                res.status(200).json(inviteResponse);
            }
        } catch (e) {
            logger.error(e.message);
            res.status(500).json({ message: 'inviteFriendController error' })
        }
    }

    editPersonalEventController = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            const editPersonalEventResult = await this.activityService.editPersonalEventServices(req.body.eventID, req.body.start_time, req.body.end_time, req.body.location, req.body.description)
            // console.log(JSON.stringify(editPersonalEventResult))
            res.status(200).json(editPersonalEventResult)
        } catch (e) {
            logger.error(e.message);
            res.status(500).json({ message: 'editPersonalEventController error' })
        }
    }
    putActivity = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            await this.activityService.updateActivity(req.body.activityID, req.body.form)
            res.status(200).json(`changing status is success`)
        } catch (e) {
            logger.error(e.message);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    deleteAvailableTimeController = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            const deleteAvailableTimeController = await this.activityService.deleteAvailableTimeServices(req.body.availabletimes_id)
            res.status(200).json(deleteAvailableTimeController)
        } catch (e) {
            logger.error(e.message);
            res.status(500).json({ message: 'DeleteAvailableTimeController error' })
        }
    }

    editAvailableTimeController = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            const { availableTimeID, start_time, end_time } = req.body
            const editAvailableTimeController = await this.activityService.editAvailableTimeServices(availableTimeID, start_time, end_time)
            res.status(200).json(editAvailableTimeController)
        } catch (e) {
            logger.error(e.message);
            res.status(500).json({ message: 'DeleteAvailableTimeController error' })
        }
    }
}