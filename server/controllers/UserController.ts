import { UserServices } from '../services/UserServices';
import { Request, Response } from 'express'
import { checkPassword } from '../hash'
import jwtSimple from 'jwt-simple'
import jwt from '../jwt';
import { hashPassword } from '../hash'
import { logger } from '../logger';

export class UserController {
    constructor(private userService: UserServices) { }

    login = async (req: Request, res: Response) => {
        try {
            // console.log(req.body)
            if (!req.body.username || !req.body.password) {
                res.status(401).json({ message: 'Missing Username / Password' });
                return;
            }
            const { username, password } = req.body;
            const user = await this.userService.getUser(username);
            // console.log(user);
            if (!user || !(await checkPassword(password, user.password))) {
                res.status(401).json({ message: 'Wrong Username / Password' })
                return;
            }
            const playload = {
                id: user.users_id,
                username: user.username
            }
            const token = jwtSimple.encode(playload, jwt.jwtSecret);
            res.json({ token })
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ message: 'internal server error1' });
        };
    };

    getUserInfo = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                res.json({ user_info: req.user });
            } else {
                res.json({ message: "No req.user" })
            }

        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ message: 'internal server error2' })
        }
    }

    register = async (req: Request, res: Response) => {
        try {
            const hash = await hashPassword(req.body.password);
            const { username, name, tel } = req.body;
            const checkDuplicate = await this.userService.getUser(username)
            if (!checkDuplicate) {
                await this.userService.register(username, hash, name, tel);
                res.status(200).json({ message: "Register Success" })
            } else {
                res.status(401).json({ message: "username duplicate" })
            }
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ message: "Register Failed" })
        }
    }

    editProfileController = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            const { users_id } = req.user
            const hash = await hashPassword(req.body.password);
            const editProfile = await this.userService.editProfileServices(users_id, req.body.name, hash, req.body.tel)
            res.status(200).json(editProfile);
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ message: "Edit Profile Failed" })
        }
    }

    putUserByFirebaseToken = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: 'Cannot find user id' })
                return
            }
            const { users_id } = req.user
            const { firebaseToken } = req.body;

            //store one only token now.need to change to store more than one token???
            await this.userService.updateUserByFirebaseToken(users_id, JSON.stringify([firebaseToken]))

            res.status(200).json({ message: "putUserByFirebaseToke successed" })
        } catch (err) {
            logger.error(err.message)
            res.status(500).json({ message: "putUserByFirebaseToken failed" })
        }
    }
};