import Knex from "knex"
import { ActicityService } from "../services/ActicityService"
import { ActivityController } from "./ActivityController"
import { Request, Response } from 'express';
// import { sendNotificationToClient } from "../firebase/notify";
// import dotenv from 'dotenv';

// dotenv.config();
const knexConfig = require('../knexfile');
const knex = Knex(knexConfig[process.env.TESTING_ENV || 'test']);

describe('Activity testing', () => {
    let controller: ActivityController
    let service: ActicityService
    let req: Request
    let res: Response
    let resJson: jest.SpyInstance;
    let resStatus: jest.SpyInstance;
    let mockSendNotificationToClient: jest.Mock


    beforeEach(function () {
        mockSendNotificationToClient = jest.fn()

        service = new ActicityService(knex)
        controller = new ActivityController(service, mockSendNotificationToClient)
        jest.spyOn(service, 'getActivityByUesrID')
        jest.spyOn(service, 'createActivity')
        jest.spyOn(service, 'inviteUser')
        jest.spyOn(service, 'selectFirebaseToken')

        req = ({
            body: {},
        } as any) as Request

        res = ({
            status: () => res,
            json: () => res,
        } as any) as Response
        resJson = jest.spyOn(res, 'json')
        resStatus = jest.spyOn(res, 'status')
    })

    afterAll(async () => {
        await knex.destroy()
        console.log("afterAll")
    })

    // describe('getActivityByUesrID', () => {
    it('test getActivityByUesrID', async () => {
        const users_id = 1
        // const activities = {
        //     initiator_user_id: "1",
        //     start_time: "2020-11-18T13:23:00.000Z",
        //     end_time: "2020-11-18T13:23:00.000Z",
        //     location: "Tsuen Wan",
        //     deadline: "2020-11-18T13:23:00.000Z",
        //     description: "football",
        //     privacy: "public"
        // }

        req = ({
            body: {},
            user: { users_id },
            query: { from: '2020-11-11T16:00:00Z', to: '2020-11-13T15:59:00Z' },
        } as any) as Request

        await controller.getActivityByUesrID(req, res)
        expect(service.getActivityByUesrID).toBeCalledWith(users_id, req.query.from, req.query.to)
        expect(resJson.mock.calls[0][0].length > 0)
        expect(resStatus).toBeCalledWith(200)

    })


    it('test getActivityByUesrID without req.query', async () => {
        req = ({
            body: {},
            user: { users_id: 1 },
        } as any) as Request

        await controller.getActivityByUesrID(req, res)
        expect(service.getActivityByUesrID).not.toBeCalled()
        expect(resJson).toBeCalledWith({ message: 'Please input the time' })
        expect(resStatus).toBeCalledWith(400)
    })
    // })


    it('test postActivity', async () => {
        const users_id = 1
        req = ({
            body: {
                description: 'football',
                start_time: '2020-11-12T12:00:00Z',
                end_time: '2020-11-12T13:00:00Z',
                deadline: '2020-11-12T13:00:00Z',
                location: 'home',
                privacy: 'public'
            },
            user: { users_id },
        } as any) as Request

        await controller.postActivity(req, res)
        expect(service.createActivity).toBeCalledWith(users_id, req.body)
        expect(typeof resJson.mock.calls[0][0][0]).toBe("number")
        expect(resJson).toBeCalledWith(expect.objectContaining([expect.any(Number)]))
        expect(resStatus).toBeCalledWith(200)
    })

    it('test postAttend', async () => {
        req = ({
            body: {
                friendUserID: 1,
                activityID: 1,
            },
            user: { name: "Jason" }
        } as any) as Request

        const notificationData = {
            title: 'Event assistance',
            body: `${req.user?.name} is inviting you.`,
            link: `/activities/${req.body.activityID}`
        }

        await controller.postAttend(req, res)
        expect(service.inviteUser).toBeCalledWith(req.body.friendUserID, req.body.activityID)
        expect(service.selectFirebaseToken).toBeCalledWith(1)
        expect(mockSendNotificationToClient).toBeCalledWith(["fake_token"], notificationData)
        expect(resJson).toBeCalledWith({ message: `inviting user is success` })
        expect(resStatus).toBeCalledWith(200)
    })

})