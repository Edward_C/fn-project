import * as Knex from "knex";
import { hashPassword } from "../hash"
import { table } from "../services/tables";
// import faker from "faker"

export async function seed(knex: Knex): Promise<void> {
    if (knex.client.config.mode !== "test") {
        return
    }
    // Deletes ALL existing entries
    const trx = await knex.transaction();

    try {
        for (const key in table) {
            await trx.raw(`TRUNCATE ${table[key]} RESTART IDENTITY CASCADE`);
        }

        const hashedPassword = await hashPassword('abcd1234')

        // Inserts seed entries
        await trx(table.USERS).insert([
            { username: "edwardc", password: hashedPassword, name: "EdwardC", tel: "98764354", firebase_token: `["fake_token"]` },
            { username: "matthew", password: hashedPassword, name: "Matthew", tel: "98764354" },
            { username: "jason", password: hashedPassword, name: "Jason", tel: "98764354" },
            { username: "alex", password: hashedPassword, name: "Alex", tel: "98764354" },
        ]);

        await trx(table.ACTIVITIES).insert(generateActivities(1));

        await trx(table.FRIENDSHIP).insert([
            { fk_user_id: 1, friend_id: 2, status: "accept" },
            { fk_user_id: 2, friend_id: 1, status: "accept" },
            { fk_user_id: 1, friend_id: 3, status: "pending" },
        ])

        await trx.commit();
    } catch (e) {
        console.log(e)
        console.log("transaction rollback")
        await trx.rollback();
    }
};


const generateActivities = (times: number) => {
    const date = "2020-11-18T13:23:00.000Z"
    const arr = []
    for (let i = 0; i < times; i++) (
        arr.push({
            initiator_user_id: "1",
            start_time: date,
            end_time: date,
            location: "Tsuen Wan",
            deadline: date,
            description: "football",
            privacy: "public"
        })
    )

    return arr
}