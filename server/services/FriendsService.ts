import Knex from 'knex';
import { table } from './tables'

export class FriendsService {
    constructor(public knex: Knex) { }

    searchUser = async (friendID: number, requestUserID: number) => {
        const checkFriendship = await this.knex(table.FRIENDSHIP).where({ 'fk_user_id': requestUserID, 'friend_id': friendID })
        if (checkFriendship.length === 1) {
            return false;
        } else {
            const user = await this.knex(table.USERS).where("users_id", friendID).returning('*');
            return user;
        }

    }

    getIDByName = async (name: string | undefined) => {
        const userID = await this.knex.select('users_id').from('users').where('name', name);
        return userID;
    }

    addFriend = async (fk_user_id: any, friend_id: any) => {
        const friendship = await this.knex(table.FRIENDSHIP).insert({ fk_user_id: fk_user_id, friend_id: friend_id, status: "pending" });
        return friendship;
    }

    getRequest = async (friend_id: number) => {
        const getRequest = await this.knex.select('friend_id', 'name', 'status', 'users_id').from(table.FRIENDSHIP).where('friend_id', friend_id).andWhere('status', 'pending').innerJoin('users', 'friendship.fk_user_id', '=', 'users.users_id')
        return getRequest;
    }

    response = async (fk_user_id: any, friend_id: any, status: string) => {
        const response = await this.knex(table.FRIENDSHIP).update({ 'status': status }).where('fk_user_id', fk_user_id).andWhere('friend_id', friend_id)
        return response;
    }

    acceptRequest = async (fk_user_id: any, friend_id: any) => {
        const trx = await this.knex.transaction()
        try {
            await this.knex(table.FRIENDSHIP).update({ 'status': 'accept' }).where('fk_user_id', fk_user_id).andWhere('friend_id', friend_id)
            const acceptRequest = await this.knex(table.FRIENDSHIP).insert({ 'fk_user_id': friend_id, 'friend_id': fk_user_id, 'status': 'accept' })
            await trx.commit();
            return acceptRequest;
        } catch (error) {
            await trx.rollback();
            throw error;
        }
    }

    rejectRequest = async (fk_user_id: any, friend_id: any) => {
        const rejectRequest = await this.knex(table.FRIENDSHIP).update({ 'status': 'reject' }).where('fk_user_id', fk_user_id).andWhere('friend_id', friend_id);
        return rejectRequest;
    }

    getFriendsList = async (fk_user_id: any) => {
        const friendsList = await this.knex.select('name', 'users_id', 'tel').from(table.FRIENDSHIP).where('fk_user_id', fk_user_id).andWhere('status', 'accept').innerJoin('users', 'friendship.friend_id', '=', 'users.users_id');
        return friendsList;
    }
}