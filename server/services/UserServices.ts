import Knex from 'knex';
import { table } from './tables'


export class UserServices {
    constructor(public knex: Knex) { }

    getUser = async (username: string) => {
        const user = await this.knex(table.USERS).where("username", username).first();
        // console.log(user)
        return user;
    }

    getUserByUserID = async (id: number) => {
        const user = await this.knex(table.USERS).where("users_id", id).first();
        // console.log(user)
        return user;
    }

    register = async (username: string, password: string, name: string, tel: number) => {
        const register = await this.knex(table.USERS).insert({
            username: username,
            password: password,
            name: name,
            tel: tel
        })
        return register;
    }

    editProfileServices = async (users_id: number, name: string, password: string, tel: number) => {
        const editProfile = await this.knex(table.USERS).where({ users_id: users_id }).update({
            name: name,
            password: password,
            tel: tel
        }).returning('name')
        return editProfile
    }

    updateUserByFirebaseToken = async (users_id: number, firebase_token: string) => {
        const id = await this.knex(table.USERS)
            .where({ users_id: users_id })
            .update({
                firebase_token
            })
            .returning('users_id')
        return id
    }
}