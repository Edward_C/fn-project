// declare global {
//     namespace Express {
//         interface Request {
//             user?: {
//                 users_id: number;
//                 username: string;
//                 name: string;
//             }
//         }
//     }
// }

export interface User {
    id: number;
    username: string;
    display_name: string;
    password: string;
    created_at: Date;
    updated_at: Date;
}

