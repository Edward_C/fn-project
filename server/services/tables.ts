export const table = Object.freeze({
    USERS: "users",
    FRIENDSHIP: "friendship",
    ACTIVITIES: "activities",
    ATTENDS: "attends",
    AVAILABLETIME: "availabletimes"
})