import Knex from "knex"
import { table } from './tables'

interface IActivity {
    start_time: string
    end_time: string
    location: string
    deadline?: string
    description: string
    privacy: string
}

interface IActivityAlone {
    description: string
    start_time: string
    end_time: string
    location: string
}

export class ActicityService {
    constructor(private knex: Knex) { }

    getActivityByUesrID = async (id: number, from: string, to: string) => {

        const selectAllexceptSomeCol = async (tables: Array<string>, exceptionsCol: Array<string>) => {
            const cols = await this.knex(this.knex.raw(`information_schema.columns`))
                .select('column_name')
                .whereIn('table_name', tables)
                .whereNotIn('column_name', exceptionsCol)

            return cols.map(col => col.column_name)
        }

        const cols = await selectAllexceptSomeCol([table.USERS, table.ACTIVITIES, table.ATTENDS], ['username', 'password', 'firebase_token'])
        // console.log(cols)
        const activities = this.knex(table.ATTENDS)
            .select([...cols, { initiator: "name" }])
            .where("fk_user_id", id)
            .where('privacy', "public")
            .whereBetween('end_time', [from, to])
            .join(table.ACTIVITIES,
                `${table.ATTENDS}.fk_activity_id`, "=",
                `${table.ACTIVITIES}.activities_id`)
            .join(table.USERS,
                `${table.USERS}.users_id`, "=",
                `${table.ACTIVITIES}.initiator_user_id`)
            .orderBy('start_time', 'desc')

        return activities
    }

    createActivity = async (initiator_user_id: number, {
        start_time, end_time, location, deadline, description, privacy
    }: IActivity) => {
        const trx = await this.knex.transaction()
        try {
            const activities_id = await this.knex.insert({
                initiator_user_id,
                start_time,
                end_time,
                location,
                deadline,
                description,
                privacy
            }).into(table.ACTIVITIES).returning("activities_id")
            const [id] = activities_id
            // console.log(activities_id)
            await this.knex.into(table.ATTENDS).insert({
                fk_user_id: initiator_user_id,
                fk_activity_id: id,
                status: "attend",
                role: "initiator"
            }).returning("attend_id")
            await trx.commit();
            return activities_id
        } catch (error) {
            await trx.rollback();
            throw error;
        }
    }

    inviteUser = async (friendUserID: number, fk_activity_id: number) => {
        const trx = await this.knex.transaction()
        try {
            const isIntvited = await this.knex(table.ATTENDS)
                .where("fk_user_id", friendUserID)
                .andWhere("fk_activity_id", fk_activity_id)
                .first()
            if (isIntvited) {
                return false
            }
            const id = this.knex.into(table.ATTENDS).insert({
                fk_user_id: friendUserID,
                fk_activity_id,
                status: "pending",
                role: "guest"
            }).returning("attend_id")
            await trx.commit();
            return id
        } catch (error) {
            await trx.rollback();
            throw error;
        }
    }

    getFriendsInvited = async (fk_user_id: number, fk_activity_id: number) => {
        const FriendsInvited = this.knex(table.ATTENDS).where('fk_activity_id', fk_activity_id)
            .select({ friendName: "name" }, { friendUserID: 'users_id' }, 'status')
            .orderBy('status')
            .join(table.USERS, `${table.ATTENDS}.fk_user_id`, '=', `${table.USERS}.users_id`)
        return FriendsInvited
    }

    getFriendsNotInvited = async (fk_user_id: number, fk_activity_id: number) => {
        const FriendsInvited = await this.knex(table.ATTENDS).select('fk_user_id').where('fk_activity_id', fk_activity_id)

        const FriendsNotInvited = await this.knex(table.FRIENDSHIP)
            .select({ friendName: "name" }, { friendUserID: 'users_id' })
            .whereNotIn('users_id', FriendsInvited.map(fd => fd.fk_user_id))
            .where('fk_user_id', fk_user_id)
            .andWhere('status', 'accept')
            .join(table.USERS, 'friendship.friend_id', '=', 'users.users_id')
        return FriendsNotInvited
    }

    createPersonalActivityServices = async (initiator_user_id: number, { start_time, end_time, location, description }: IActivityAlone) => {
        const trx = await this.knex.transaction()
        try {
            const createPersonalActivity = await this.knex.insert({
                initiator_user_id: initiator_user_id,
                start_time: start_time, end_time: end_time,
                location: location, description: description,
                privacy: 'private'
            }).into(table.ACTIVITIES).returning("activities_id")
            const [id] = createPersonalActivity;
            await this.knex(table.ATTENDS).insert({ fk_user_id: initiator_user_id, fk_activity_id: id, status: "attend", role: "initiator" })
            await trx.commit();
            return createPersonalActivity;
        } catch (error) {
            await trx.rollback();
            throw error;
        }
    }

    getPersonalActivitiesByDateServices = async (fk_user_id: number, { start_time, end_time }: IActivityAlone) => {
        const getActivitiesAloneByDateServices = await this.knex(table.ATTENDS)
            .where('start_time', '>=', start_time)
            .andWhere('end_time', '<=', end_time)
            .andWhere('fk_user_id', fk_user_id)
            .andWhere('status', 'attend')
            .innerJoin('activities', 'attends.fk_activity_id', '=', 'activities.activities_id')
            .orderBy('start_time')
        return getActivitiesAloneByDateServices;
    }

    deletePersonalActivitiesServices = async (activities_id: number) => {
        const trx = await this.knex.transaction()
        try {
            const deletePersonalActivitiesServices = await this.knex(table.ATTENDS).where('fk_activity_id', activities_id).del();
            await this.knex(table.ACTIVITIES).where(' activities_id', activities_id).del();
            return deletePersonalActivitiesServices;
        } catch (error) {
            await trx.rollback(); throw error
        }
    }

    putAttend = async (fk_user_id: number, fk_activity_id: number, status: string) => {
        const id = await this.knex(table.ATTENDS)
            .update('status', status)
            .where("fk_user_id", fk_user_id)
            .where("fk_activity_id", fk_activity_id)
        return id
    }

    getFriendActivitiesServices = async (friendsID: number, start_time: string, end_time: string) => {
        const getFriendActivitiesServices = await this.knex.select('*').from('activities').innerJoin('attends', 'activities.activities_id', '=', 'attends.fk_activity_id').where({
            'initiator_user_id': friendsID,
            'status': 'attend'
        }).andWhere('start_time', '>=', start_time).andWhere('end_time', '<=', end_time);
        return getFriendActivitiesServices;
    }

    getInitiatorByActivityID = async (initiator_user_id: number, activities_id: number) => {
        const isInitiator = await this.knex(table.ACTIVITIES)
            .where('initiator_user_id', '=', initiator_user_id)
            .andWhere('activities_id', '=', activities_id)
            .first()
        return isInitiator
    }

    getFreeTime = async (userID: number, { start_time, end_time }: IActivity) => {
        const freetime = await this.knex(table.ACTIVITIES).
            join(table.ATTENDS,
                `${table.ATTENDS}.fk_activity_id`, "=",
                `${table.ACTIVITIES}.activities_id`)
            .where("fk_user_id", userID)
            .where('status', "=", "attend")
            .andWhere(function () {
                this.orWhereBetween('start_time', [start_time, end_time])
                    .orWhereBetween('end_time', [start_time, end_time])
                    .orWhere(function () {
                        this.where('start_time', '<', start_time).andWhere('end_time', '>', end_time)
                    })
            })
            .first()

        return freetime
    }

    setAvailableTimeServices = async (fk_user_id: number, start_time: string, end_time: string) => {
        const availableTime = await this.knex(table.AVAILABLETIME).insert({ fk_user_id: fk_user_id, start_time: start_time, end_time: end_time }).returning(" availabletimes_id");
        return availableTime;
    }

    getAvailableTimeServices = async (fk_user_id: number, start_time: string, end_time: string) => {
        const availableTime = await this.knex(table.AVAILABLETIME).where('fk_user_id', '=', fk_user_id).andWhere('start_time', '>=', start_time).andWhere('end_time', '<=', end_time);
        return availableTime;
    }

    getDeadline = async (activityID: number, now: string) => {
        const isDeadline = await this.knex(table.ACTIVITIES)
            .where('activities_id', activityID)
            .where('deadline', '<', now)
            .first()
        return isDeadline
    }

    editPersonalEventServices = async (activities_id: number, start_time: string, end_time: string, location: string, description: string) => {
        const editPersonalEvent = await this.knex(table.ACTIVITIES).update({ start_time: start_time, end_time: end_time, location: location, description: description }).where({ activities_id: activities_id })
        return editPersonalEvent;
    }

    updateActivity = async (activities_id: number, {
        start_time, end_time, location, deadline, description
    }: IActivity) => {
        const id = await this.knex(table.ACTIVITIES)
            .update({ start_time, end_time, location, deadline, description }).where({ activities_id })
        return id;
    }

    deleteAvailableTimeServices = async (availabletimes_id: number) => {
        const deleteAvailableTimeServices = await this.knex(table.AVAILABLETIME).where('availabletimes_id', availabletimes_id).del();
        return deleteAvailableTimeServices;
    }

    editAvailableTimeServices = async (availabletimes_id: number, start_time: string, end_time: string) => {
        const deleteAvailableTimeServices = await this.knex(table.AVAILABLETIME).update({ start_time: start_time, end_time: end_time }).where({ availabletimes_id: availabletimes_id })
        return deleteAvailableTimeServices
    }

    selectFirebaseToken = async (user_id: number) => {
        const token = await this.knex(table.USERS).select('firebase_token')
            .where("users_id", user_id)
            // .whereNotNull('firebase_token')
            .first()
        return token
    }
}