import express from 'express';
import { activityController, isInitiator } from '../main';
import { isLoggedIn } from '../main';


export const activityRoutes = express.Router();

// activityRoutes.get('/holding', isLoggedIn, activityController.getHoldingActivityByDate)
activityRoutes.get('/', isLoggedIn, activityController.getActivityByUesrID)
activityRoutes.post('/', isLoggedIn, activityController.postActivity)
activityRoutes.post('/personal', isLoggedIn, activityController.createPersonalActivityController)
activityRoutes.post('/personal/getActivities', isLoggedIn, activityController.getPersonalActivitiesByDateController)
activityRoutes.post('/attend', isLoggedIn, isInitiator, activityController.postAttend)
activityRoutes.delete('/personal/deleteActivity', isLoggedIn, isInitiator, activityController.deletePersonalActivitiesController)
activityRoutes.put('/attend', isLoggedIn, activityController.putAttend)
activityRoutes.post('/friend/getActivities', isLoggedIn, activityController.getFriendActivitiesController)
activityRoutes.get('/friendsattends', isLoggedIn, activityController.getFriendsattends)
activityRoutes.post('/availabletime', isLoggedIn, activityController.setAvailableTimeController)
activityRoutes.post('/getavailabletime', isLoggedIn, activityController.getAvailableTimeController)
activityRoutes.post('/getFriendAvailabletime', isLoggedIn, activityController.getFriendAvailableTimeController)
activityRoutes.post('/inviteFriend', isLoggedIn, activityController.inviteFriendController)
activityRoutes.put('/editPersonalEvent', isLoggedIn, activityController.editPersonalEventController)
activityRoutes.put('/', isLoggedIn, isInitiator, activityController.putActivity)
activityRoutes.delete('/deleteAvailableTime', isLoggedIn, activityController.deleteAvailableTimeController)
activityRoutes.put('/editAvailableTime', isLoggedIn, activityController.editAvailableTimeController)