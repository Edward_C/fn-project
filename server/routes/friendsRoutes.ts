import express from 'express';
import { friendsController } from '../main'
import { isLoggedIn } from '../main'

export const friendsRoutes = express.Router();

friendsRoutes.post('/search', isLoggedIn, friendsController.search);
friendsRoutes.post('/addFriends', isLoggedIn, friendsController.addFriends);
friendsRoutes.get('/getRequest', isLoggedIn, friendsController.getRequest);
friendsRoutes.put('/acceptRequest', isLoggedIn, friendsController.acceptRequest);
friendsRoutes.put('/rejectRequest', isLoggedIn, friendsController.rejectRequest);
friendsRoutes.get('/friendsList', isLoggedIn, friendsController.friendsList);