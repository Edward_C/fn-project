import express from 'express';
import { userController, isLoggedIn } from '../main';

export const userRoutes = express.Router();

userRoutes.post('/login', userController.login);
userRoutes.get('/info', isLoggedIn, userController.getUserInfo);
userRoutes.post('/register', userController.register)
userRoutes.put('/editProfile', isLoggedIn, userController.editProfileController)
userRoutes.put('/firebase-token', isLoggedIn, userController.putUserByFirebaseToken)