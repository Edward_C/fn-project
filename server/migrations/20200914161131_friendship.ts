import * as Knex from "knex";

const friendship = "friendship"

export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable(friendship)) {
        return;
    } else {
        await knex.schema.createTable(friendship, (table) => {
            table.increments("friendship_id");
            table.integer("fk_user_id");
            table.integer("friend_id");
            table.string("status");
            table.foreign("fk_user_id").references("users.users_id");
            table.foreign("friend_id").references("users.users_id")
        })
    }
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(friendship);
}

