import * as Knex from "knex";

const activities = "activities"

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(activities);
    if (hasTable) {
        return knex.schema.table(activities, (table) => {
            table.string('privacy');
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(activities);
    if (hasTable) {
        return knex.schema.table(activities, (table) => {
            table.dropColumn('privacy');
        })
    }
}

