import * as Knex from "knex";

const available = "availabletimes"


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable(available)) {
        return;
    } else {
        await knex.schema.createTable(available, (table) => {
            table.increments("availabletimes_id");
            table.integer("fk_user_id")
            table.dateTime("start_time");
            table.dateTime("end_time");
            table.foreign("fk_user_id").references("users.users_id")
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(available);
}

