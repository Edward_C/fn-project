import * as Knex from "knex";

const activities = "activities"

export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable(activities)) {
        return;
    } else {
        await knex.schema.createTable(activities, (table) => {
            table.increments("activities_id");
            table.integer("initiator");
            table.dateTime("start_time");
            table.dateTime("end_time");
            table.string("location");
            table.foreign("initiator").references("users.users_id")
        })
    }
}


export async function down(knex: Knex): Promise<void> {
}

