import * as Knex from "knex";


const activities = "activities"

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(activities);
    if (hasTable) {
        return knex.schema.alterTable(activities, (table) => {
            table.dateTime("timer");
            table.string("description")
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(activities);
    if (hasTable) {
        return knex.schema.alterTable(activities, (table) => {
            table.increments("activities_id");
            table.integer("initiator");
            table.dateTime("start_time");
            table.dateTime("end_time");
            table.string("location");
            table.foreign("initiator").references("users.users_id")
        })
    }
}

