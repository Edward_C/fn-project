import * as Knex from "knex";

const usersTable = "users"

export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable(usersTable)) {
        return;
    } else {
        await knex.schema.createTable(usersTable, (table) => {
            table.increments("users_id");
            table.string("username").notNullable().unique();
            table.string("password").notNullable();
            table.string("name").notNullable().unique();
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(usersTable);
}

