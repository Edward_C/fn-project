import * as Knex from "knex";

const attends = "attends";

export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable(attends)) {
        return;
    } else {
        await knex.schema.createTable(attends, (table) => {
            table.increments("attend_id");
            table.integer("fk_user_id");
            table.integer("fk_activity_id");
            table.string("status")
            table.foreign("fk_user_id").references("users.users_id");
            table.foreign("fk_activity_id").references("activities.activities_id")
        })
    }
}


export async function down(knex: Knex): Promise<void> {
}

