import * as Knex from "knex";

const table = "users";

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(table)
    if (hasTable) {
        return knex.schema.table(table, (table) => {
            table.string('firebase_token')
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(table)
    if (hasTable) {
        return knex.schema.table(table, (table) => {
            table.dropColumn('firebase_token')
        })
    }
}