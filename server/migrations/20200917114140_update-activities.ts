import * as Knex from "knex";

const activities = "activities"

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(activities);
    if (hasTable) {
        return knex.schema.table(activities, (table) => {
            table.renameColumn("initiator", "initiator_user_id")
            table.renameColumn("timer", "deadline")
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(activities);
    if (hasTable) {
        return knex.schema.table(activities, (table) => {
            table.renameColumn("initiator_user_id", "initiator")
            table.renameColumn("deadline", "timer")
        })
    }
}

