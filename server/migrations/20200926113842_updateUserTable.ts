import * as Knex from "knex";

const table = "users";

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(table);
    if (hasTable) {
        return knex.schema.table(table, (table) => {
            table.integer('tel')
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(table);
    if (hasTable) {
        return knex.schema.table(table, (table) => {
            table.increments("users_id");
            table.string("username").notNullable().unique();
            table.string("password").notNullable();
            table.string("name").notNullable().unique();
        })
    }
}

