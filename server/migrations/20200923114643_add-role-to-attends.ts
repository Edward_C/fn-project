import * as Knex from "knex";

const table = 'attends'

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(table);
    if (hasTable) {
        return knex.schema.table(table, (t) => {
            t.string('role');
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(table);
    if (hasTable) {
        return knex.schema.table(table, (t) => {
            t.dropColumn('role');
        })
    }
}

