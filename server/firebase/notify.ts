import { messaging } from './firebaseInit';

export const sendNotificationToClient = (tokens: string[], data: { [key: string]: string; }) => {
    // Send a message to the devices corresponding to the provided
    // registration tokens.
    console.log("sendNotificationToClient@@@")
    messaging
        .sendMulticast({ tokens, data })
        .then(response => {
            // Response is an object of the form { responses: [] }
            const successes = response.responses.filter(r => r.success === true)
                .length;
            const failures = response.responses.filter(r => r.success === false)
                .length;
            console.log(
                'Notifications sent:',
                `${successes} successful, ${failures} failed`
            );
        })
        .catch(error => {
            console.log('Error sending message:', error);
        });
};