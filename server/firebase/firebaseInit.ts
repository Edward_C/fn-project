import admin from 'firebase-admin';
import { googleApplicationCredentials } from '../settings'

const serviceAccount = googleApplicationCredentials


admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://tecky-fn-project.firebaseio.com'
});

export const messaging = admin.messaging();