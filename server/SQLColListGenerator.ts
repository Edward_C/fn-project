// import { knex } from './main'
import Knex from 'knex';
import dotenv from 'dotenv';
import { table } from './services/tables'

dotenv.config();

const knexConfig = require('./knexfile');
export const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])


const getTableName = async (tables: Array<string>, exceptions: Array<string>) => {
    let columnNames: Array<string> = []
    for (const table of tables) {
        const columnInfo = await knex(table).columnInfo()
        columnNames = columnNames.concat(Object.keys(columnInfo))
    }

    for (const exception of exceptions) {
        columnNames = columnNames.filter((name) => name !== exception)
    }


    console.log(columnNames)
    knex.destroy()
}

getTableName([table.USERS, table.ATTENDS], ['password', 'users_id'])