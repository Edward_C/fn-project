import express from 'express';
import bodyParser from 'body-parser';
import { logger } from './logger';
import cors from "cors";
import Knex from 'knex';
import { sendNotificationToClient } from './firebase/notify';
// import dotenv from 'dotenv';

// dotenv.config();

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const knexConfig = require('./knexfile');
export const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])

app.use(cors());

declare global {
    namespace Express {
        interface Request {
            user?: {
                users_id: number;
                username: string;
                name: string;
            }
        }
    }
}

import { UserServices } from './services/UserServices';
import { UserController } from './controllers/UserController';
import { ActicityService } from './services/ActicityService';
import { ActivityController } from './controllers/ActivityController';
import { FriendsService } from './services/FriendsService';
import { FriendsController } from './controllers/FriendsController'


const userService = new UserServices(knex);
export const userController = new UserController(userService);

const activityService = new ActicityService(knex)
export const activityController = new ActivityController(activityService, sendNotificationToClient)

const friendsService = new FriendsService(knex);
export const friendsController = new FriendsController(friendsService);

import { createIsLoggedIn, createIsInitiator } from './guards';
export const isLoggedIn = createIsLoggedIn(userService);
export const isInitiator = createIsInitiator(activityService);

import { routes } from './routes'


const API_VERSION = '/api/v1';
app.use(API_VERSION, routes)


const PORT = 8080;
app.listen(PORT, () => {
    logger.info(`Server listing on http://localhost:${PORT}`);
})