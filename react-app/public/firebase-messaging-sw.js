// Import and configure the Firebase SDK
// These scripts are made available when the app is served or deployed on Firebase Hosting
// If you do not serve/host your project using Firebase Hosting see https://firebase.google.com/docs/web/setup
importScripts('https://www.gstatic.com/firebasejs/7.21.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.21.1/firebase-messaging.js');

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const config = {
    apiKey: "AIzaSyCH4IEGbmb72UkhTxY7Y8aRnjss3_4Oup4",
    authDomain: "tecky-fn-project.firebaseapp.com",
    databaseURL: "https://tecky-fn-project.firebaseio.com",
    projectId: "tecky-fn-project",
    storageBucket: "tecky-fn-project.appspot.com",
    messagingSenderId: "766331322897",
    appId: "1:766331322897:web:56ba9f1d0c3d4c6610a7b0",
    measurementId: "G-54KMFZWCTR"
};
// Initialize Firebase
firebase.initializeApp(config);
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    const notificationTitle = payload.data.title;
    const notificationOptions = {
        body: payload.data.body,
        icon: '/firebase-logo.png'
    };

    //redirect
    self.addEventListener('notificationclick', function (event) {
        event.waitUntil(self.clients.openWindow(payload.data.link));
        event.notification.close();
    })

    return self.registration.showNotification(notificationTitle,
        notificationOptions);
});

self.addEventListener('notificationclick', event => {
    console.log(event)
    return event;
});


