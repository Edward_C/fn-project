import { IUserInfo } from "./state";

export const LOGIN_PROCESSING = '@@auth/LOGIN_PROCESSING';
export const LOGIN_SUCCESS = '@@auth/LOGIN_SUCCESS';
export const LOGIN_FAIL = '@@auth/LOGIN_FAIL';
export const LOGIN_USER = '@@auth/LOGIN_USER';
export const LOGOUT_SUCCESS = '@@auth/LOGOUT_SUCCESS';
export const EDIT_IS_PROCESSING = '@@auth/EDIT_IS_PROCESSING';
export const EDIT_IS_NOT_PROCESSING = '@@/EDIT_IS_NOT_PROCESSING'

interface ILoginProcessing {
    type: typeof LOGIN_PROCESSING;
}

interface ILoginSuccess {
    type: typeof LOGIN_SUCCESS;
};

interface ILoginFail {
    type: typeof LOGIN_FAIL;
    message: string;
}

interface ILoginUser {
    type: typeof LOGIN_USER;
    user: IUserInfo;
}

interface ILogoutSuccess {
    type: typeof LOGOUT_SUCCESS;
}

interface IEditIsProcessing {
    type: typeof EDIT_IS_PROCESSING
}

interface IEditIsNotProcessing {
    type: typeof EDIT_IS_NOT_PROCESSING
}

export const loginProcessing = (): ILoginProcessing => {
    return {
        type: LOGIN_PROCESSING,
    }
}

export const loginSuccess = (): ILoginSuccess => {
    return {
        type: LOGIN_SUCCESS,
    };
};

export const loginFail = (message: string): ILoginFail => {
    return {
        type: LOGIN_FAIL,
        message,
    }
}

export const loginUser = (user: IUserInfo): ILoginUser => {
    return {
        type: LOGIN_USER,
        user
    }
}

export const logoutSuccess = (): ILogoutSuccess => {
    return {
        type: LOGOUT_SUCCESS,
    }
}

export const editIsProcessing = (): IEditIsProcessing => {
    return {
        type: EDIT_IS_PROCESSING
    }
}

export const editIsNotProcessing = (): IEditIsNotProcessing => {
    return {
        type: EDIT_IS_NOT_PROCESSING
    }
}

export type IAuthActions = ILoginProcessing | ILoginSuccess | ILoginFail | ILoginUser | ILogoutSuccess | IEditIsProcessing | IEditIsNotProcessing;