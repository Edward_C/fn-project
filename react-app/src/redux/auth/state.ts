export interface IUserInfo {
    users_id: number;
    name: string;
    tel: number
}

export interface IAuthState {
    isAuthenticated: boolean | null;
    isProcessing: boolean;
    editProcessing: boolean
    userInfo: IUserInfo | null;
    errMessage: string;
}



export const initAuthState: IAuthState = {
    isAuthenticated: null,
    isProcessing: false,
    editProcessing: false,
    userInfo: null,
    errMessage: ''
}