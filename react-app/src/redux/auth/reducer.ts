import { IAuthState, initAuthState } from './state'
import { IAuthActions, LOGIN_PROCESSING, LOGIN_SUCCESS, LOGIN_FAIL, LOGIN_USER, LOGOUT_SUCCESS } from './actions'
import { EDIT_IS_PROCESSING, EDIT_IS_NOT_PROCESSING } from './actions'
export const authReducers = (state: IAuthState = initAuthState, action: IAuthActions): IAuthState => {
    switch (action.type) {
        case LOGIN_PROCESSING:
            return {
                ...state,
                isAuthenticated: false,
                errMessage: '',
                isProcessing: true
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                isProcessing: false,
                isAuthenticated: true,
            };
        case LOGIN_FAIL:
            return {
                ...state,
                isProcessing: false,
                isAuthenticated: false,
                errMessage: action.message
            }
        case LOGIN_USER:
            return {
                ...state,
                isProcessing: false,
                isAuthenticated: true,
                userInfo: action.user
            }
        case LOGOUT_SUCCESS:
            return {
                ...state,
                isAuthenticated: false,
                userInfo: null
            }
        case EDIT_IS_PROCESSING:
            return {
                ...state,
                editProcessing: true
            }
        case EDIT_IS_NOT_PROCESSING:
            return {
                ...state,
                editProcessing: false
            }
        default:
            return state;
    }
}