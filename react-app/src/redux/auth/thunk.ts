import { Dispatch } from 'react';
import { IAuthActions, loginProcessing, loginFail, loginSuccess, loginUser, logoutSuccess } from './actions';
import { editIsProcessing, editIsNotProcessing } from './actions'
import { push, CallHistoryMethodAction } from "connected-react-router";
import { IFriendsActions } from '../friends/actions';
import { requestFirebaseNotificationPermission } from '../../firebaseInit';

const { REACT_APP_API_SERVER } = process.env;

interface editProfileForm {
    name: string
    password: string
    tel: number
}

export const restoreLoginThunk = () => {
    return async (dispatch: Dispatch<IAuthActions | CallHistoryMethodAction>) => {
        const token = localStorage.getItem('token')
        if (token) {
            const res = await fetch(`${REACT_APP_API_SERVER}/user/info`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            if (res.status === 200) {
                dispatch(loginSuccess());
            } else {
                dispatch(loginFail(''))
                return
            }
            const user = await res.json()
            dispatch(loginUser(user.user_info))
        } else {
            dispatch(loginFail(""))
        }
    }
}

export const loginThunk = (username: string, password: string) => {
    return async (dispatch: Dispatch<IAuthActions | IFriendsActions | CallHistoryMethodAction>) => {
        dispatch(loginProcessing());
        const res = await fetch(`${REACT_APP_API_SERVER}/user/login`, {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ username, password })
        })
        const data = await res.json();
        if (res.status === 200) {
            localStorage.setItem('token', data.token);
            dispatch(loginSuccess());
            dispatch(push('/'))
        } else {
            dispatch(loginFail(data.message));
        }
    }
}

export function logoutThunk() {
    return async (dispatch: Dispatch<IAuthActions | CallHistoryMethodAction>) => {
        dispatch(logoutSuccess());
        localStorage.removeItem('token');
        dispatch(push('/'));
    };
}

export const editProfileThunk = (editProfileData: editProfileForm) => {
    return async (dispatch: Dispatch<IAuthActions | IFriendsActions | CallHistoryMethodAction>) => {
        dispatch(editIsProcessing())
        const token = localStorage.getItem('token')
        if (token) {
            const res = await fetch(`${REACT_APP_API_SERVER}/user/editProfile`, {
                method: "PUT",
                headers: { Authorization: `Bearer ${token}`, 'Content-Type': 'application/json' },
                body: JSON.stringify(editProfileData)
            })
            await res.json();
            dispatch(editIsNotProcessing())
            // dispatch(loginUser(editProfileResult[0]))
        } else {
            dispatch(loginFail(""))
        }
    }
}

export const putUserbyFirebaseTokenThunk = () => {
    return async (dispatch: Dispatch<IAuthActions>) => {
        try {
            const token = localStorage.getItem('token')
            if (!token) {
                dispatch(loginFail(''));
                return
            }
            const firebaseToken = await requestFirebaseNotificationPermission()

            await fetch(`${REACT_APP_API_SERVER}/user/firebase-token`, {
                method: "PUT",
                headers: { Authorization: `Bearer ${token}`, 'Content-Type': 'application/json' },
                body: JSON.stringify({ firebaseToken })
            })
        } catch (error) {
            console.log(error)
        }

    }
}