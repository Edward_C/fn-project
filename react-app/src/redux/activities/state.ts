export interface IActivity {
    activities_id: number
    initiator: string
    start_time: string
    end_time: string
    description: string
    deadline: string
    location: string
    privacy: string
    status: string
    role: string
}
export interface IAttend {
    friendName: string
    friendUserID: number
    status?: string
}

export interface IAvailableTimetable {
    availabletimes_id: number
    start_time: string
    end_time: string
}

export interface IActivitiesRange {
    from: string
    to: string
}

export interface IActivitiesState {
    activities: Array<IActivity>
    personalActivities: Array<IActivity>
    friendActivities: Array<IActivity>
    friendsInvited: Array<IAttend>
    friendsNotInvited: Array<IAttend>
    availabletimes: Array<IAvailableTimetable>
    friendAvailableTime: Array<IAvailableTimetable>
    isProcessing: boolean
    activitiesRange: IActivitiesRange
}

export const initActivityState: IActivitiesState = {
    activities: [],
    personalActivities: [],
    friendActivities: [],
    friendsInvited: [],
    friendsNotInvited: [],
    availabletimes: [],
    friendAvailableTime: [],
    isProcessing: false,
    activitiesRange: { from: "", to: "" }
}