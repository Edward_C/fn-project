import { ACTIVITIES_IS_NOT_PROCESSING, ACTIVITIES_IS_PROCESSING, getFriendActivities, getPersonalActivities, setActivities, setAttends } from "./action"
import { activitiesReducer } from "./reducer"
import { IActivitiesState, initActivityState } from "./state";

describe('activities reducer', () => {
    let initialState: IActivitiesState;

    it('setActivitiesIsProcessing', () => {
        const state = activitiesReducer(undefined, { type: ACTIVITIES_IS_PROCESSING })

        initialState = { ...initActivityState, isProcessing: true }

        expect(state).toEqual(initialState)
    })

    it('setActivitiesIsNotProcessing', () => {
        const state = activitiesReducer(undefined, { type: ACTIVITIES_IS_NOT_PROCESSING })

        initialState = { ...initActivityState }

        expect(state).toEqual(initialState)
    })

    it('setActivities', () => {
        const activities = [{
            activities_id: 1,
            initiator: "matthew",
            start_time: "21:00",
            end_time: "21:00",
            description: "dinner",
            deadline: "21:00",
            location: "mk",
            privacy: "public",
            status: "pending",
            role: "initiator",
        }]

        const state = activitiesReducer(undefined, setActivities(activities))

        initialState = { ...initActivityState, activities }

        expect(state).toEqual(initialState)
    })

    it('setAttends', () => {
        const friendsInvited = [{
            friendName: "Amy",
            friendUserID: 1,
            status: "attend"
        }]
        const friendsNotInvited = [{
            friendName: "Ben",
            friendUserID: 2,
        }]

        const state = activitiesReducer(undefined, setAttends(friendsInvited, friendsNotInvited))

        initialState = { ...initActivityState, friendsInvited, friendsNotInvited }

        expect(state).toEqual(initialState)
    })

    it('getPersonalActivities', () => {
        const personalActivities = [{
            activities_id: 1,
            initiator: "matthew",
            start_time: "21:00",
            end_time: "21:00",
            description: "dinner",
            deadline: "21:00",
            location: "mk",
            privacy: "public",
            status: "pending",
            role: "initiator",
        }]


        const state = activitiesReducer(undefined, getPersonalActivities(personalActivities))

        initialState = { ...initActivityState, personalActivities }

        expect(state).toEqual(initialState)
    })

    it('getFriendActivities', () => {
        const friendActivities = [{
            activities_id: 1,
            initiator: "matthew",
            start_time: "21:00",
            end_time: "21:00",
            description: "dinner",
            deadline: "21:00",
            location: "mk",
            privacy: "public",
            status: "pending",
            role: "initiator",
        }]


        const state = activitiesReducer(undefined, getFriendActivities(friendActivities))

        initialState = { ...initActivityState, friendActivities }

        expect(state).toEqual(initialState)
    })

})