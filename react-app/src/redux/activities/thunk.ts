import { Dispatch } from "redux"
import { IRootState } from "../store"
import { ICreateActivityFormInput } from "../../pages/CreateActivitiesPage"
import { IAuthActions, loginFail } from "../auth/actions"
import {
    setActivitiesIsProcessing, setActivitiesIsNotProcessing, setActivities, getPersonalActivities,
    getFriendActivities, setAttends, getAvailableTime, getFriendAvailableTime, IActivitiesActions
} from "./action"
import { push } from "connected-react-router"
import { message } from 'antd';
import moment from 'moment'

const { REACT_APP_API_SERVER } = process.env

export interface createPersonalActivity {
    description: string
    start_time: string
    end_time: string
    location: string
}

interface FriendsActivities {
    friendsID: any
    start_time: any
    end_time: any
}

interface AvailableTime {
    user_id?: number
    start_time: string
    end_time: string
}

export interface editPersonalActivity {
    eventID: number
    start_time?: string
    end_time?: string
    location?: string
    description?: string
}

interface editAvailableTime {
    availableTimeID: number
    start_time: string
    end_time: string
}

export const createPersonalActivityThunk = (formInput: createPersonalActivity) => {
    return async (dispatch: Dispatch, getState: () => IRootState) => {
        const token = localStorage.getItem('token')
        if (!token) {
            dispatch(loginFail(''));
            return
        }
        formInput.start_time = (moment(formInput.start_time).utc().format())
        formInput.end_time = (moment(formInput.end_time).utc().format())
        dispatch(setActivitiesIsProcessing())
        const res = await fetch(`${REACT_APP_API_SERVER}/activity/personal`, {
            method: "POST",
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify(formInput)
        })
        const id = await res.json()
        dispatch(setActivitiesIsNotProcessing())
        console.log(id)
    }
}

export const getPersonalActivityThunk = (start_time: any, end_time: any) => {
    return async (dispatch: Dispatch, getState: () => IRootState) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
            return;
        }
        const res = await fetch(`${REACT_APP_API_SERVER}/activity/personal/getActivities`, {
            method: "POST",
            headers: { Authorization: `Bearer ${token}`, "Content-Type": "application/json" },
            body: JSON.stringify({ start_time, end_time })
        })
        const personalActivity = await res.json();
        // console.log(JSON.stringify(personalActivity));
        dispatch(getPersonalActivities(personalActivity))
    }
}

export const deletePersonalActivityThunk = (activityID: number) => {
    return async (dispatch: Dispatch, getState: () => IRootState) => {
        dispatch(setActivitiesIsProcessing())
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
            return;
        }
        const res = await fetch(`${REACT_APP_API_SERVER}/activity/personal/deleteActivity`, {
            method: "DELETE",
            headers: { Authorization: `Bearer ${token}`, "Content-Type": "application/json" },
            body: JSON.stringify({ activityID })
        })
        await res.json();
        dispatch(setActivitiesIsNotProcessing())
        // console.log(`deletePersonalActivityThunk - ${JSON.stringify(deleteActivity)}`);
    }
}

export const createActivityThunk = (formInput: ICreateActivityFormInput) => {
    return async (dispatch: Dispatch, getState: () => IRootState) => {
        dispatch(setActivitiesIsProcessing())
        const token = localStorage.getItem('token')
        if (!token) {
            dispatch(loginFail(''));
            return
        }
        formInput.start_time = (moment(formInput.start_time).utc().format())
        formInput.end_time = (moment(formInput.end_time).utc().format())
        formInput.deadline = (moment(formInput.deadline).utc().format())
        console.log(formInput.start_time)
        const res = await fetch(`${REACT_APP_API_SERVER}/activity`, {
            method: "POST",
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify(formInput)
        })
        const result = await res.json()
        if (res.status === 200) {
            dispatch(setActivitiesIsNotProcessing())
            dispatch(push(`/activities/${result}`))
            return
        }
        if (res.status === 403) {
            message.error('you already having another activity in this period');
        }
    }
}

export const inviteUserThunk = (friendUserID: number, activityID: number) => {
    return async (dispatch: Dispatch) => {
        const token = localStorage.getItem('token')
        if (!token) {
            dispatch(loginFail(''));
            return
        }
        dispatch(setActivitiesIsProcessing())
        const res = await fetch(`${REACT_APP_API_SERVER}/activity/attend`, {
            method: "POST",
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ friendUserID, activityID })
        })
        const data = await res.json()
        dispatch(setActivitiesIsNotProcessing())
        console.log(data)
    }
}

export const getActivitiesThunk = ({ from, to }: any) => {
    return async (dispatch: Dispatch) => {
        const token = localStorage.getItem('token')
        if (!token) {
            dispatch(loginFail(''));
            return
        }
        from = (moment(from).utc().format())
        to = (moment(to).utc().format())
        // console.log(from, to)
        const range = `from=${from}&to=${to}`
        const res = await fetch(`${REACT_APP_API_SERVER}/activity?${range}`, {
            headers: {
                Authorization: `Bearer ${token}`,
            }
        });

        const activities = await res.json()
        // console.log(activities)
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            dispatch(setActivities(activities));
        }
    }
}

export const putAttendThunk = (activityID: number, status: string) => {
    return async (dispatch: Dispatch) => {
        const token = localStorage.getItem('token')
        if (!token) {
            dispatch(loginFail(''));
            return
        }
        dispatch(setActivitiesIsProcessing())
        const now = moment().utc().format("YYYY-MM-DD HH:mm")
        const res = await fetch(`${REACT_APP_API_SERVER}/activity/attend`, {
            method: "PUT",
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ activityID, status, now })
        });
        const result = await res.json()
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 403) {
            message.error(result.message);
        }
        dispatch(setActivitiesIsNotProcessing())
    }
}

export const getFriendsActivitiesThunk = (formInput: FriendsActivities) => {
    return async (dispatch: Dispatch, getState: () => IRootState) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
            return;
        }
        const res = await fetch(`${REACT_APP_API_SERVER}/activity/friend/getActivities`, {
            method: "POST",
            headers: { Authorization: `Bearer ${token}`, "Content-Type": "application/json" },
            body: JSON.stringify(formInput)
        })
        const friendActivities = await res.json();
        dispatch(getFriendActivities(friendActivities));
        // console.log(JSON.stringify(friendActivities))
    }
}

export const getAttendsThunk = (activityID: number) => {
    return async (dispatch: Dispatch, getState: () => IRootState) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
            return;
        }
        const res = await fetch(`${REACT_APP_API_SERVER}/activity/friendsattends?activityID=${activityID}`, {
            headers: { Authorization: `Bearer ${token}`, "Content-Type": "application/json" },
        })
        const friends = await res.json();
        const { friendsInvited, friendsNotInvited } = friends

        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            dispatch(setAttends(friendsInvited, friendsNotInvited))
        }
    }
}

export const setAvailableTimeThunk = (formInput: AvailableTime) => {
    return async (dispatch: Dispatch, getState: () => IRootState) => {
        dispatch(setActivitiesIsProcessing())
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
            return;
        }
        // console.log(moment().format())
        // console.log(formInput.start_time)
        // console.log(moment(formInput.start_time).format())
        // console.log(moment(formInput.start_time).utc().format())
        formInput.start_time = moment(formInput.start_time).utc().format()
        formInput.end_time = (moment(formInput.end_time).utc().format())
        dispatch(setActivitiesIsProcessing())
        const res = await fetch(`${REACT_APP_API_SERVER}/activity/availabletime`, {
            method: "POST",
            headers: { Authorization: `Bearer ${token}`, "Content-Type": "application/json" },
            body: JSON.stringify(formInput)
        })
        await res.json();
        dispatch(setActivitiesIsNotProcessing())
        // console.log(availabletime);
        dispatch(setActivitiesIsNotProcessing())
    }
}

export const getAvailableTimeThunk = (formInput: AvailableTime) => {
    return async (dispatch: Dispatch, getState: () => IRootState) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
            return;
        }
        const res = await fetch(`${REACT_APP_API_SERVER}/activity/getavailabletime`, {
            method: "POST",
            headers: { Authorization: `Bearer ${token}`, "Content-Type": "application/json" },
            body: JSON.stringify(formInput)
        })
        const availabletime = await res.json();
        dispatch(getAvailableTime(availabletime));
    }
}

export const getFriendAvailableTimeThunk = (user_id: number, start_time: any, end_time: any) => {
    return async (dispatch: Dispatch, getState: () => IRootState) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
            return;
        }
        const res = await fetch(`${REACT_APP_API_SERVER}/activity/getFriendAvailabletime`, {
            method: "POST",
            headers: { Authorization: `Bearer ${token}`, "Content-Type": "application/json" },
            body: JSON.stringify({ user_id, start_time, end_time })
        })
        const getFriendAvailableTimeRes = await res.json();
        dispatch(getFriendAvailableTime(getFriendAvailableTimeRes));
    }
}

export const inviteFriendThunk = (friendID: number, description: string, start_time: string, end_time: string, deadline: string, privacy: string, location: string) => {
    return async (dispatch: Dispatch, getState: () => IRootState) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
            return;
        }
        start_time = (moment(start_time).utc().format())
        end_time = (moment(end_time).utc().format())
        deadline = (moment(deadline).utc().format())
        const res = await fetch(`${REACT_APP_API_SERVER}/activity/inviteFriend`, {
            method: "POST",
            headers: { Authorization: `Bearer ${token}`, "Content-Type": "application/json" },
            body: JSON.stringify({ friendID, description, start_time, end_time, deadline, privacy, location })
        })
        await res.json();
        // console.log(inviteFriend);
    }
}

export const editPersonalEventThunk = (editFormInput: editPersonalActivity) => {
    return async (dispatch: Dispatch<IActivitiesActions | IAuthActions>) => {
        dispatch(setActivitiesIsProcessing())
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
            return;
        }
        editFormInput.start_time = (moment(editFormInput.start_time).utc().format())
        editFormInput.end_time = (moment(editFormInput.end_time).utc().format())
        const res = await fetch(`${REACT_APP_API_SERVER}/activity/editPersonalEvent`, {
            method: "PUT",
            headers: { Authorization: `Bearer ${token}`, "Content-Type": "application/json" },
            body: JSON.stringify(editFormInput)
        })
        await res.json();
        dispatch(setActivitiesIsNotProcessing())
        // console.log(editPersonalEventResult);
    }
}

export const putActivityThunk = (activityID: number, form: ICreateActivityFormInput) => {
    return async (dispatch: Dispatch, getState: () => IRootState) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
            return;
        }
        dispatch(setActivitiesIsProcessing())
        form.start_time = (moment(form.start_time).utc().format())
        form.end_time = (moment(form.end_time).utc().format())
        form.deadline = (moment(form.deadline).utc().format())
        const res = await fetch(`${REACT_APP_API_SERVER}/activity`, {
            method: "PUT",
            headers: { Authorization: `Bearer ${token}`, "Content-Type": "application/json" },
            body: JSON.stringify({ activityID, form })
        })
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        dispatch(setActivitiesIsNotProcessing())
    }
}

export const deleteAvailableTimeThunk = (availabletimes_id: number) => {
    return async (dispatch: Dispatch, getState: () => IRootState) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
            return;
        }
        dispatch(setActivitiesIsProcessing())
        const res = await fetch(`${REACT_APP_API_SERVER}/activity/deleteAvailableTime`, {
            method: "DELETE",
            headers: { Authorization: `Bearer ${token}`, "Content-Type": "application/json" },
            body: JSON.stringify({ availabletimes_id })
        })
        await res.json();
        dispatch(setActivitiesIsNotProcessing())
        // console.log(deleteAvailableTimeResult)
    }
}

export const editAvailableTimeThunk = (editAvailableTime: editAvailableTime) => {
    return async (dispatch: Dispatch, getState: () => IRootState) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
            return;
        }
        editAvailableTime.start_time = (moment(editAvailableTime.start_time).utc().format())
        editAvailableTime.end_time = (moment(editAvailableTime.end_time).utc().format())
        dispatch(setActivitiesIsProcessing())
        const res = await fetch(`${REACT_APP_API_SERVER}/activity/editAvailableTime`, {
            method: "PUT",
            headers: { Authorization: `Bearer ${token}`, "Content-Type": "application/json" },
            body: JSON.stringify(editAvailableTime)
        })
        await res.json();
        dispatch(setActivitiesIsNotProcessing());
        // console.log(editAvailableTimeResult)
    }
}