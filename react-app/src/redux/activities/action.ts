import { IActivity, IAttend, IAvailableTimetable, IActivitiesRange } from "./state";

export const ACTIVITIES_IS_PROCESSING = "@@activities/ACTIVITIES_IS_PROCESSING"
export const ACTIVITIES_IS_NOT_PROCESSING = "@@activities/ACTIVITIES_IS_NOT_PROCESSING"
export const SET_ACTIVITIES = "@@activities/SET_ACTIVITIES"
export const SET_ATTENDS = "@@activities/SET_ATTENDS"
export const GET_PERSONAL_ACTIVITIES = "@@activities/GET_PERSONAL_ACTIVITIES"
export const GET_FRIEND_ACTIVITIES = "@@activities/GET_FRIEND_ACTIVITIES"
export const GET_AVAILABLETIME = "@@activities/GET_AVAILABLETIME"
export const GET_FRIEND_AVAILABLETIME = "@@activities/GET_FRIEND_AVAILABLETIME"
export const SET_ACTIVITIES_RANGE = "@@activities/SET_ACTIVITIES_RANGE"


interface IActivitiesIsProcessing {
    type: typeof ACTIVITIES_IS_PROCESSING
}
interface IActivitiesIsNotProcessing {
    type: typeof ACTIVITIES_IS_NOT_PROCESSING
}
interface ISetActivities {
    type: typeof SET_ACTIVITIES
    activities: Array<IActivity>
}
interface ISetAttends {
    type: typeof SET_ATTENDS
    friendsInvited: Array<IAttend>
    friendsNotInvited: Array<IAttend>
}

interface IGetPersonalActivities {
    type: typeof GET_PERSONAL_ACTIVITIES
    personalActivities: Array<IActivity>
}

interface IGetFriendActivities {
    type: typeof GET_FRIEND_ACTIVITIES
    friendActivities: Array<IActivity>
}

interface IGetAvailableTime {
    type: typeof GET_AVAILABLETIME
    availabletimes: Array<IAvailableTimetable>
}

interface IGetFriendAvailableTime {
    type: typeof GET_FRIEND_AVAILABLETIME
    friendAvailableTime: Array<IAvailableTimetable>
}

interface ISetActivitiesRange {
    type: typeof SET_ACTIVITIES_RANGE
    activitiesRange: IActivitiesRange
}



export const setActivitiesIsProcessing = (): IActivitiesIsProcessing => {
    return {
        type: ACTIVITIES_IS_PROCESSING,
    }
}
export const setActivitiesIsNotProcessing = (): IActivitiesIsNotProcessing => {
    return {
        type: ACTIVITIES_IS_NOT_PROCESSING,
    }
}
export const setActivities = (activities: Array<IActivity>): ISetActivities => {
    return {
        type: SET_ACTIVITIES,
        activities
    }
}
export const setAttends = (friendsInvited: Array<IAttend>, friendsNotInvited: Array<IAttend>): ISetAttends => {
    return {
        type: SET_ATTENDS,
        friendsInvited,
        friendsNotInvited,
    }
}

export const getPersonalActivities = (personalActivities: Array<IActivity>): IGetPersonalActivities => {
    return {
        type: GET_PERSONAL_ACTIVITIES,
        personalActivities
    }
}

export const getFriendActivities = (friendActivities: Array<IActivity>): IGetFriendActivities => {
    return {
        type: GET_FRIEND_ACTIVITIES,
        friendActivities
    }
}

export const getAvailableTime = (availabletimes: Array<IAvailableTimetable>): IGetAvailableTime => {
    return {
        type: GET_AVAILABLETIME,
        availabletimes
    }
}

export const getFriendAvailableTime = (friendAvailableTime: Array<IAvailableTimetable>): IGetFriendAvailableTime => {
    return {
        type: GET_FRIEND_AVAILABLETIME,
        friendAvailableTime
    }
}

export const setActivitiesRange = (activitiesRange: IActivitiesRange): ISetActivitiesRange => {
    return {
        type: SET_ACTIVITIES_RANGE,
        activitiesRange
    }
}

export type IActivitiesActions = IActivitiesIsProcessing | IActivitiesIsNotProcessing |
    ISetActivities | IGetPersonalActivities | IGetFriendActivities | ISetAttends | IGetAvailableTime | IGetFriendAvailableTime
    | ISetActivitiesRange
