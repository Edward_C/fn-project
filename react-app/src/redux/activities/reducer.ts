import { IActivitiesState, initActivityState } from "./state";
import {
    IActivitiesActions, ACTIVITIES_IS_PROCESSING, ACTIVITIES_IS_NOT_PROCESSING, SET_ACTIVITIES,
    GET_PERSONAL_ACTIVITIES, GET_FRIEND_ACTIVITIES, SET_ATTENDS, GET_AVAILABLETIME, GET_FRIEND_AVAILABLETIME, SET_ACTIVITIES_RANGE
} from "./action";

export const activitiesReducer = (
    state: IActivitiesState = initActivityState,
    action: IActivitiesActions
): IActivitiesState => {
    switch (action.type) {
        case SET_ACTIVITIES:
            return {
                ...state,
                activities: action.activities,
            }
        case SET_ATTENDS:
            return {
                ...state,
                friendsInvited: action.friendsInvited,
                friendsNotInvited: action.friendsNotInvited,
            }
        case ACTIVITIES_IS_PROCESSING:
            return {
                ...state,
                isProcessing: true,
            }
        case ACTIVITIES_IS_NOT_PROCESSING:
            return {
                ...state,
                isProcessing: false,
            }
        case GET_PERSONAL_ACTIVITIES:
            return {
                ...state,
                personalActivities: action.personalActivities
            }
        case GET_FRIEND_ACTIVITIES:
            return {
                ...state,
                friendActivities: action.friendActivities
            }
        case GET_AVAILABLETIME:
            return {
                ...state,
                availabletimes: action.availabletimes
            }
        case GET_FRIEND_AVAILABLETIME:
            return {
                ...state,
                friendAvailableTime: action.friendAvailableTime
            }
        case SET_ACTIVITIES_RANGE:
            return {
                ...state,
                activitiesRange: action.activitiesRange
            }
        default:
            return state
    }
}