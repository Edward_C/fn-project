import { combineReducers, createStore, applyMiddleware, compose } from "redux"
import { RouterState, connectRouter, routerMiddleware, CallHistoryMethodAction } from 'connected-react-router';
import { createBrowserHistory } from "history"
import thunk, { ThunkDispatch as OldThunkDispatch } from 'redux-thunk';
// import logger from 'redux-logger';
import { IAuthState } from './auth/state';
import { IAuthActions } from './auth/actions';
import { authReducers } from './auth/reducer'
import { IActivitiesActions } from "./activities/action";
import { IActivitiesState } from "./activities/state";
import { activitiesReducer } from "./activities/reducer";
import { IFriendsState } from './friends/state';
import { IFriendsActions } from './friends/actions';
import { friendsReducers } from './friends/reducer'
import { IAddFriendsState } from "./addFriends/state";
import { addFriendsReducers } from "./addFriends/reducer";
import { IAddFriendsActions } from "./addFriends/actions";

export const history = createBrowserHistory();

export interface IRootState {
    friends: IFriendsState
    addFriends: IAddFriendsState
    auth: IAuthState
    activities: IActivitiesState
    router: RouterState
}

export type IRootAction = IAuthActions | IActivitiesActions | IFriendsActions | IAddFriendsActions | CallHistoryMethodAction

const rootReducer = combineReducers<IRootState>({
    friends: friendsReducers,
    addFriends: addFriendsReducers,
    auth: authReducers,
    activities: activitiesReducer,
    router: connectRouter(history)
})

declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
    }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export type ThunkDispatch = OldThunkDispatch<IRootState, null, IRootAction>

export default createStore<IRootState, IRootAction, {}, {}>(
    rootReducer, composeEnhancers(
        applyMiddleware(thunk),
        applyMiddleware(routerMiddleware(history)),
        // applyMiddleware(logger)
    ))