import { Dispatch } from 'react';
import { IFriendsActions, getFriendsList, friendsResponseIsProcessing, friendsResponseIsNotProcessing } from './actions';
import { friendsRequestPending } from './actions';
import { CallHistoryMethodAction } from 'connected-react-router';
import { IAuthActions } from '../auth/actions';


const { REACT_APP_API_SERVER } = process.env;


export const getRequest = () => {
    return async (dispatch: Dispatch<IFriendsActions | IAuthActions | CallHistoryMethodAction>) => {
        try {
            const token = localStorage.getItem('token');
            const res = await fetch(`${REACT_APP_API_SERVER}/friends/getRequest`, {
                headers: { Authorization: `Bearer ${token}` }
            });
            const getRequestResult = await res.json();
            dispatch(friendsRequestPending(getRequestResult));
        } catch (err) {
            console.log(`AddFriendsThunks - ${err}`)
        }
    }
}

export const accept = (requestUser: string, responseStatus: string) => {
    return async (dispatch: Dispatch<IFriendsActions | CallHistoryMethodAction>) => {
        try {
            const token = localStorage.getItem('token');
            dispatch(friendsResponseIsProcessing())
            const res = await fetch(`${REACT_APP_API_SERVER}/friends/acceptRequest`, {
                method: "PUT",
                headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${token}` },
                body: JSON.stringify({ requestUser, responseStatus })
            })
            await res.json();
            dispatch(friendsResponseIsNotProcessing())
            // console.log(acceptRequestResult)
        } catch (err) {
            console.log(`AddFriendsThunks - ${err}`)
        }
    }
}

export const reject = (requestUser: string, responseStatus: string) => {
    return async (dispatch: Dispatch<IFriendsActions | CallHistoryMethodAction>) => {
        try {
            const token = localStorage.getItem('token');
            dispatch(friendsResponseIsProcessing())
            const res = await fetch(`${REACT_APP_API_SERVER}/friends/rejectRequest`, {
                method: "PUT",
                headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${token}` },
                body: JSON.stringify({ requestUser, responseStatus })
            })
            dispatch(friendsResponseIsNotProcessing())
            await res.json();
        } catch (err) {
            console.log(`AddFriendsThunks - ${err}`)
        }
    }
}

export const friendsListThunk = () => {
    return async (dispatch: Dispatch<IFriendsActions | CallHistoryMethodAction>) => {
        try {
            const token = localStorage.getItem('token');
            const res = await fetch(`${REACT_APP_API_SERVER}/friends/friendsList`, {
                headers: { Authorization: `Bearer ${token}` }
            })
            const friendsListResult = await res.json();
            dispatch(getFriendsList(friendsListResult))
        } catch (err) {
            console.log(`friendsListThunk - ${err}`)
        }
    }
}