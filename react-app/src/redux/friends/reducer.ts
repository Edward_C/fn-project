import { IFriendsActions } from './actions';
import { FRIENDS_REQUEST_PENDING, FRIENDS_RESPONSE, FRIENDS_RESPONSE_IS_PROCESSING, FRIENDS_RESPONSE_IS_NOT_PROCESSING, GET_FRIENDS_LIST } from './actions'
import { IFriendsState, initFriendsState } from './state';

export const friendsReducers = (state: IFriendsState = initFriendsState, action: IFriendsActions): IFriendsState => {
    switch (action.type) {
        case FRIENDS_REQUEST_PENDING:
            return {
                ...state,
                friendsRequestList: action.friendsRequestList
            }
        case FRIENDS_RESPONSE:
            return {
                ...state,
                friendsRequestList: action.friendsResponse
            }
        case FRIENDS_RESPONSE_IS_PROCESSING:
            return {
                ...state,
                isProcessing: true,
            }
        case FRIENDS_RESPONSE_IS_NOT_PROCESSING:
            return {
                ...state,
                isProcessing: false,
            }
        case GET_FRIENDS_LIST:
            return {
                ...state,
                friendsList: action.friendsList
            }
        default:
            return state;
    }
}