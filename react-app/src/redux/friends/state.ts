export interface IFriendsList {
    users_id: number
    name: string;
    tel: number;
}

export interface IFriendRequestList {
    friend_id: number;
    users_id: number;
    name: string;
    status: string;
}

export interface IFriendsState {
    friendsList: Array<IFriendsList>
    friendsRequestList: Array<IFriendRequestList>
    isProcessing: boolean
}

export const initFriendsState: IFriendsState = {
    friendsList: [],
    friendsRequestList: [],
    isProcessing: false,
}