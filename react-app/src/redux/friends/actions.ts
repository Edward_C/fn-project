import { IFriendRequestList, IFriendsList } from "./state";

export const FRIENDS_REQUEST_PENDING = '@@friends/FRIENDS_REQUEST_PENDING';
export const FRIENDS_RESPONSE = '@@friends/FRIENDS_RESPONSE';
export const FRIENDS_RESPONSE_IS_PROCESSING = "friends/FRIENDS_RESPONSE_IS_PROCESSING"
export const FRIENDS_RESPONSE_IS_NOT_PROCESSING = "friends/FRIENDS_RESPONSE_IS_NOT_PROCESSING"
export const GET_FRIENDS_LIST = '@@friends/GET_FRIENDS_LIST';


interface IFriendsRequestPending {
    type: typeof FRIENDS_REQUEST_PENDING;
    friendsRequestList: Array<IFriendRequestList>
}

interface IFriendsResponse {
    type: typeof FRIENDS_RESPONSE;
    friendsResponse: Array<IFriendRequestList>
}

interface IFriendsResponseIsProcessing {
    type: typeof FRIENDS_RESPONSE_IS_PROCESSING
}

interface IFriendsResponseIsNotProcessing {
    type: typeof FRIENDS_RESPONSE_IS_NOT_PROCESSING
}

interface IGetFriendsList {
    type: typeof GET_FRIENDS_LIST;
    friendsList: Array<IFriendsList>
}

export const friendsRequestPending = (friendsRequestList: Array<IFriendRequestList>): IFriendsRequestPending => {
    return {
        type: FRIENDS_REQUEST_PENDING,
        friendsRequestList
    }
}

export const friendsResponse = (friendsResponse: Array<IFriendRequestList>): IFriendsResponse => {
    return {
        type: FRIENDS_RESPONSE,
        friendsResponse
    }
}

export const friendsResponseIsProcessing = (): IFriendsResponseIsProcessing => {
    return {
        type: FRIENDS_RESPONSE_IS_PROCESSING
    }
}

export const friendsResponseIsNotProcessing = (): IFriendsResponseIsNotProcessing => {
    return {
        type: FRIENDS_RESPONSE_IS_NOT_PROCESSING
    }
}

export const getFriendsList = (friendsList: Array<IFriendsList>): IGetFriendsList => {
    return {
        type: GET_FRIENDS_LIST,
        friendsList
    }
}

export type IFriendsActions = IFriendsRequestPending | IFriendsResponse | IFriendsResponseIsProcessing | IFriendsResponseIsNotProcessing | IGetFriendsList;