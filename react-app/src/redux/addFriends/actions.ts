export const FRIENDS_GET_SUCCESS = '@@addFriends/FRIENDS_GET_SUCCESS';

interface IFriendsGet {
    type: typeof FRIENDS_GET_SUCCESS;
    friendsGet: string;
}

export const friendsGet = (friendsGet: string): IFriendsGet => {
    return {
        type: FRIENDS_GET_SUCCESS,
        friendsGet,
    }
}

export type IAddFriendsActions = IFriendsGet;