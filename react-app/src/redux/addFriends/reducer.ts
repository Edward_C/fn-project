import { IAddFriendsState, initFiendState } from './state'
import { IAddFriendsActions, FRIENDS_GET_SUCCESS } from './actions'

export const addFriendsReducers = (state: IAddFriendsState = initFiendState, action: IAddFriendsActions): IAddFriendsState => {
    switch (action.type) {
        case FRIENDS_GET_SUCCESS:
            return {
                friendsGet: action.friendsGet,
            }
        default:
            return state;
    }
}