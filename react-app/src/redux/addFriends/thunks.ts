import { Dispatch } from 'react';
import { IAddFriendsActions, friendsGet } from './actions';
import { CallHistoryMethodAction } from "connected-react-router";

const { REACT_APP_API_SERVER } = process.env;

export const searchThunk = (searchBox: string) => {
    return async (dispatch: Dispatch<IAddFriendsActions | CallHistoryMethodAction>) => {
        try {
            const token = localStorage.getItem('token')
            const res = await fetch(`${REACT_APP_API_SERVER}/friends/search`, {
                method: "POST",
                headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${token}` },
                body: JSON.stringify(searchBox)
            })
            const friends = await res.json();
            console.log(friends)
            dispatch(friendsGet(friends[0].name));
        } catch (err) {
            console.log(err)
        }
    }
}

export const addFriendsThunk = (friendsName: string) => {
    return async (dispatch: Dispatch<IAddFriendsActions | CallHistoryMethodAction>) => {
        try {
            const token = localStorage.getItem('token')
            const res = await fetch(`${REACT_APP_API_SERVER}/friends/addFriends`, {
                method: "POST",
                headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${token}` },
                body: JSON.stringify(friendsName)
            })
            const addFriendsResult = await res.json();
            console.log(addFriendsResult)
        } catch (err) {
            console.log(err)
        }
    }
}
