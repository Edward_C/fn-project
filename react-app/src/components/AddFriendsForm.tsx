import React, { useState } from 'react'
import { Container, Row, Col, Form } from 'reactstrap';
import { useForm } from "react-hook-form";
import '../css/AddFriendsForm.css'

import { addFriendsThunk, searchThunk } from '../redux/addFriends/thunks'
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../redux/store';


const AddFriendsForm: React.FC = () => {
    const onSubmit2 = (data: any) => {
        setAddedFriend(true)
        dispatch(addFriendsThunk(data));
    };
    const dispatch = useDispatch();
    const { register, handleSubmit } = useForm();
    const friendsGet = useSelector((state: IRootState) => state.addFriends.friendsGet)
    const [addedFriend, setAddedFriend] = useState(false)

    const onSubmit1 = (data: any) => {
        dispatch(searchThunk(data))
        setAddedFriend(false)
    };


    return (
        <Container>
            <Row>
                <Col>
                    <Form onSubmit={handleSubmit(onSubmit1)}>
                        <Row>
                            <Col className="searchForm">
                                <input className="searchInput" name="searchBox" ref={register} />
                                <input className="searchInputBtn" type="submit" value="Search" />
                            </Col>
                        </Row>
                    </Form>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Form onSubmit={handleSubmit(onSubmit2)}>
                        {friendsGet &&
                            <Row className="addFriendsForm">
                                <Col className="searchResult-Col">
                                    <div className="searchResult" >{friendsGet}</div>
                                    <input type="hidden" name="addFriendsName" value={friendsGet} ref={register} />
                                </Col>
                                <Col className="formInputBtn-Col">
                                    {!addedFriend && <input className="formInputBtn" type="submit" value="AddFriends" />}
                                    {addedFriend && <div>Requested</div>}
                                </Col>
                            </Row>
                        }
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}

export default AddFriendsForm
