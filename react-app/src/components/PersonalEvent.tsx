import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Container, Row, Col, Button, Modal, ModalBody, Form } from "reactstrap";
import moment from "moment";
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import '../css/Timetable.css'
import { IRootState } from '../redux/store';
import { deletePersonalActivityThunk, editPersonalEventThunk } from '../redux/activities/thunk'
import { useForm } from 'react-hook-form';


interface ITimetableProps {
    date?: any
    name?: string
}

const PersonalEvent: React.FC<ITimetableProps> = (props) => {
    const dispatch = useDispatch()
    const { register, handleSubmit } = useForm();
    const [editForm, setEditForm] = useState(false);
    const clickEditBtn = () => setEditForm(!editForm);
    const personalActivities = useSelector((state: IRootState) => state.activities.personalActivities);
    const deleteActivity = (id: number) => {
        dispatch(deletePersonalActivityThunk(id))
    }

    const submitEditForm = (data: any) => {
        const eventID = data.eventID;
        const description = data.description;
        const location = data.location;
        const start_time = (moment(props.date).format().slice(0, 11) + data.start_time + ":00+08");
        const end_time = (moment(props.date).format().slice(0, 11) + data.end_time + ":00+08");
        dispatch(editPersonalEventThunk({ eventID, start_time, end_time, location, description }));
    }

    return (
        <Container className="themed-container" fluid={true}>
            <Row className="personal_timeTableRow">
                <Col className="personal_timeTable">

                    <Row className="timeTable-list-header">
                        <Col xs="3" md="4" className="timeTable-time">Time</Col>
                        <Col xs="3" md="4" className="timeTable-description">Description</Col>
                        <Col xs="3" md="3" className="timeTable-location">Location</Col>
                        <Col xs="3" md="1"></Col>
                    </Row>
                    {personalActivities.length > 0 && personalActivities.map(personalActivity => (
                        <div key={personalActivity.activities_id}>
                            <Row className="timeTable-activities-item" id={`${personalActivity.activities_id}`}>
                                <Col xs="3" md="4">
                                    <span>{moment(personalActivity.start_time).format('HH:mm')}</span>
                                    <span>-</span>
                                    <span>{moment(personalActivity.end_time).format('HH:mm')}</span>
                                </Col>
                                <Col xs="3" md="4">{personalActivity.description}</Col>
                                <Col xs="3" md="3">{personalActivity.location}</Col>
                                <Col xs="3" md="1">
                                    <DeleteOutlined className="deleteBtn" onClick={() => deleteActivity(personalActivity.activities_id)} />
                                    <EditOutlined onClick={clickEditBtn} />
                                </Col>
                            </Row>
                            <Row><Col>
                                <Modal isOpen={editForm} toggle={clickEditBtn} className="editForm">
                                    <Form onSubmit={handleSubmit(submitEditForm)}>
                                        <ModalBody className="editFormBody">
                                            <Row><Col className="editFormHeader">Edit event</Col></Row>
                                            <Row><Col className="editFormDate">{props.date}</Col></Row>
                                            <Row>
                                                <Col className="editFormTitle">Time : </Col>
                                                <Col><input type="time" ref={register} name="start_time" /></Col>
                                                <Col className="editFormTo">to</Col>
                                                <Col><input type="time" ref={register} name="end_time" /></Col>
                                            </Row>
                                            <Row className="editFormGroup">
                                                <Col>Description : </Col>
                                                <Col><input className="editFormInput" type="text" ref={register} name="description" /></Col>
                                            </Row>
                                            <Row className="editFormGroup">
                                                <Col>Location : </Col>
                                                <Col><input className="editFormInput" type="text" ref={register} name="location" /></Col>
                                            </Row>
                                            <Row>
                                                <Col><input type="hidden" ref={register} name="eventID" value={personalActivity.activities_id} /></Col>
                                                <Col className="editFormBtnCol"><Button className="editFormBtn" type="submit" onClick={() => setEditForm(false)}>Submit</Button>{' '}</Col>
                                                <Col className="editFormBtnCol"><Button className="editFormBtn" onClick={clickEditBtn}>Cancel</Button></Col>
                                                <Col></Col>
                                            </Row>
                                        </ModalBody>
                                    </Form>
                                </Modal>
                            </Col></Row>
                        </div>
                    ))}
                </Col>
            </Row>
        </Container>
    )
}

export default PersonalEvent
