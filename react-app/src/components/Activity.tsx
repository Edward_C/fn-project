import React from 'react';
import { Col, Row } from 'reactstrap';
// import { Button } from '@material-ui/core';
// import { push } from 'connected-react-router';
import { IActivity } from '../redux/activities/state';
import moment from 'moment'

interface IActivityProps {
    activity: IActivity
    button?: Element | any
    // activityOnClick: () => void
}

interface IActivityRowProps {
    title: string
    status: string | number
}


const Activity: React.FC<IActivityProps> = (props) => {
    const { activity, button } = props

    // const startDate = moment(activity.start_time).format("YYYY-MM-DD")
    // const startTime = moment(activity.start_time).format("HH:mm")
    // const endDate = moment(activity.end_time).format("YYYY-MM-DD")
    // const endTime = moment(activity.end_time).format("HH:mm")
    const start = moment(activity.start_time).format("YYYY-MM-DD HH:mm")
    const end = moment(activity.end_time).format("YYYY-MM-DD HH:mm")
    const deadLine = moment(activity.deadline).format("YYYY-MM-DD HH:mm")
    const timer = moment(activity.start_time).fromNow()
    // console.log(moment(activity.start_time).fromNow())

    return (
        <>
            {<Row className="activities">
                <Col>
                    <Row>
                        <Col sm="10">
                            <ActivityRow title="Initiator" status={activity.initiator} />
                            <ActivityRow title="Description" status={activity.description} />
                            <ActivityRow title="Location" status={activity.location} />
                            <ActivityRow title="Start" status={`${start} (${timer})`} />
                            <ActivityRow title="End" status={end} />
                            <ActivityRow title="Deadline" status={deadLine} />

                        </Col>
                        <Col sm="2" className="button">
                            {button}
                        </Col>
                    </Row>
                    {props.children}
                </Col>
            </Row>}
        </>)
}

export default Activity


const ActivityRow: React.FC<IActivityRowProps> = (props) => {
    return (
        <Row>
            <Col xs="6" md="3">{props.title}:</Col>
            <Col xs="6" md="9" className="act-col">{props.status}</Col>
        </Row>
    )
}