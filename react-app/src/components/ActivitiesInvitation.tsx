import React, { useState, useEffect } from 'react';
import Badge from "antd/lib/badge";
import EventIcon from '@material-ui/icons/Event';
import { getActivitiesThunk } from "../redux/activities/thunk";
import { IActivity } from "../redux/activities/state";
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import { Col, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Row } from 'reactstrap';
import { push } from 'connected-react-router';
import { CheckCircleTwoTone, CloseCircleTwoTone } from '@ant-design/icons';
import { putAttendThunk } from '../redux/activities/thunk';

const ActivitiesInvitation: React.FC = () => {
    const dispatch = useDispatch();
    const [dropdownOpen, setDropdownOpen] = useState(false);
    const [invitations, setInvitations] = useState<null | Array<IActivity>>(null);
    const toggle = () => setDropdownOpen(prevState => !prevState);
    const activities = useSelector((state: IRootState) => state.activities.activities)
    const isProcessing = useSelector((state: IRootState) => state.activities.isProcessing)

    useEffect(() => {
        const range = { from: "2010-01-01T00:00", to: "2030-12-31T23:59" }
        if (!isProcessing) {
            dispatch(getActivitiesThunk(range))
        }
    }, [dispatch, isProcessing])

    useEffect(() => {
        const invitations = activities.filter(activity => activity.status === "pending")
        setInvitations(invitations)
    }, [activities])

    return (
        <>
            {invitations &&
                <Dropdown isOpen={dropdownOpen} toggle={toggle}>
                    <DropdownToggle className="navbar-btn">
                        <Badge count={invitations.length} showZero className="navbar-badge" ><EventIcon /></Badge>
                    </DropdownToggle>
                    <DropdownMenu className="navbar-act-menu">
                        {invitations.length > 0 && invitations.map(activity => (
                            <>
                                <DropdownItem
                                    key={activity.activities_id}>
                                    <div onClick={() => dispatch(push(`/activities/${activity.activities_id}`))}>
                                        <div><img src={`https://api.adorable.io/avatars/10/${activity.initiator}.png`} alt="" />
                                            <span>&nbsp;&nbsp;&nbsp;</span>
                                            {activity.initiator}</div>
                                        <div><EventIcon />
                                            <span>&nbsp;&nbsp;&nbsp;</span>
                                            {activity.description}</div>
                                    </div>
                                    <Row>
                                        <Col>
                                            <CheckCircleTwoTone className="act-icon" twoToneColor="#52c41a"
                                                onClick={() => {
                                                    dispatch(putAttendThunk(activity.activities_id, 'attend'))
                                                    dispatch(push(`/activities/${activity.activities_id}`))
                                                }} />
                                        </Col>
                                        <Col>
                                            <CloseCircleTwoTone className="act-icon" twoToneColor="#c41a1a"
                                                onClick={() => dispatch(putAttendThunk(activity.activities_id, 'refuse'))} />
                                        </Col>
                                    </Row>
                                </DropdownItem>
                            </>))}
                        {invitations.length === 0 &&
                            <DropdownItem>
                                <div>No Invitations</div>
                            </DropdownItem>}
                    </DropdownMenu>
                </Dropdown>}
        </>
    )
}

export default ActivitiesInvitation