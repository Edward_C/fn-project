import React, { useState } from 'react';
import { EditTwoTone, DeleteTwoTone } from '@ant-design/icons';
import { useDispatch } from 'react-redux';
import { IActivity } from '../redux/activities/state';
import { Popconfirm, message } from 'antd';
import { deletePersonalActivityThunk, putActivityThunk } from '../redux/activities/thunk';
import { push } from 'connected-react-router';
import { Modal, ModalBody } from 'reactstrap';
import { Row, Col, Form, FormGroup, Label, Input } from "reactstrap"
import { Button } from '@material-ui/core';
import { useForm } from "react-hook-form"
import moment from 'moment'
import { ICreateActivityFormInput } from '../pages/CreateActivitiesPage';

interface IProps {
    activity: IActivity
    className?: string
}

const ActManageBtn: React.FC<IProps> = ({ activity }) => {
    const dispatch = useDispatch();
    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);
    const { register, handleSubmit } = useForm<ICreateActivityFormInput>()
    const now = moment().format("YYYY-MM-DDTHH:mm")
    const [endTimeMin, setEndTimeMin] = useState(now);
    const [deadLineMax, setDeadLineMax] = useState("");
    const onSubmit = (data: ICreateActivityFormInput) => {
        dispatch(putActivityThunk(activity.activities_id, data))
        toggle()
    }

    function confirm() {
        dispatch(deletePersonalActivityThunk(activity.activities_id))
        message.success('Click on Delete');
        dispatch(push(`/activities`))
    }

    function cancel() {
        // message.error('Click on No');
    }

    return (
        <>
            <Row>
                <Col>
                    <EditTwoTone onClick={toggle} className="act-icon" />
                    <Modal isOpen={modal} toggle={toggle} className="edit-act-form">
                        <ModalBody>
                            <Form className="create-activitive-form" onSubmit={handleSubmit(onSubmit)}>
                                <FormGroup>
                                    <Row>
                                        <Col className="createEventTitle"><Label for="description">Description : </Label></Col>
                                        <Col><Input type="text"
                                            className="timeInput"
                                            name="description"
                                            placeholder="description"
                                            defaultValue={activity.description}
                                            innerRef={register({
                                                required: "Required"
                                            })} /></Col>
                                    </Row>
                                </FormGroup>
                                <FormGroup>
                                    <Row>
                                        <Col className="createEventTitle"><Label for="start_time">Start time :</Label></Col>
                                        <Col><Input type="datetime-local"
                                            className="timeInput"
                                            name="start_time"
                                            placeholder="start time"
                                            defaultValue={moment(activity.start_time).format("YYYY-MM-DDTHH:mm")}
                                            min={`${now}`}
                                            onChange={(e) => setEndTimeMin(e.target.value)}
                                            innerRef={register({
                                                required: "Required"
                                            })} /></Col>
                                    </Row>
                                </FormGroup>
                                <FormGroup>
                                    <Row>
                                        <Col className="createEventTitle"><Label for="end_time">End time :</Label></Col>
                                        <Col><Input type="datetime-local"
                                            className="timeInput"
                                            name="end_time"
                                            placeholder="end time"
                                            defaultValue={moment(activity.end_time).format("YYYY-MM-DDTHH:mm")}
                                            min={`${endTimeMin}`}
                                            onChange={(e) => setDeadLineMax(e.target.value)}
                                            innerRef={register({
                                                required: "Required"
                                            })} /></Col>
                                    </Row>
                                </FormGroup>
                                <FormGroup>
                                    <Row>
                                        <Col className="createEventTitle"><Label for="deadline">Join deadline :</Label></Col>
                                        <Col><Input type="datetime-local"
                                            className="timeInput"
                                            name="deadline"
                                            placeholder="deadline"
                                            defaultValue={moment(activity.deadline).format("YYYY-MM-DDTHH:mm")}
                                            min={`${now}`}//2020-09-18T15:55
                                            max={`${deadLineMax}`}
                                            innerRef={register({
                                                required: "Required"
                                            })} /></Col>
                                    </Row>
                                </FormGroup>
                                <FormGroup>
                                    <Row>
                                        <Col className="createEventTitle"><Label for="location">Location :</Label></Col>
                                        <Col><Input type="text"
                                            className="timeInput"
                                            name="location"
                                            placeholder="location"
                                            defaultValue={activity.location}
                                            innerRef={register({
                                                required: "Required"
                                            })} /></Col>
                                    </Row>
                                </FormGroup>
                                <FormGroup>
                                    <Row>
                                        <Col>
                                            <Button onClick={toggle} variant="contained" >Cancel</Button>
                                        </Col>
                                        <Col>
                                            <Button type="submit" value="Submit" variant="contained">Edit</Button>
                                        </Col>
                                    </Row>
                                </FormGroup>
                            </Form>
                        </ModalBody>
                    </Modal>
                </Col>
                <Col>
                    <Popconfirm
                        title="Are you sure delete this task?"
                        onConfirm={confirm}
                        onCancel={cancel}
                        okText="Yes"
                        cancelText="No"
                    >
                        <DeleteTwoTone className="act-icon" />
                    </Popconfirm>
                </Col>
            </Row>
        </>
    )
}

export default ActManageBtn