import React, { useEffect } from 'react'
import { Container, Row, Col, Form, Modal, ModalBody } from "reactstrap";
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import moment from 'moment'
import './FriendsCalendar'
import '../css/FriendTimetable.css'
import { useState } from 'react';
import { CloseOutlined } from '@ant-design/icons'
import Calendar from './FriendsCalendar';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import { getFriendAvailableTimeThunk, getPersonalActivityThunk, inviteFriendThunk } from '../redux/activities/thunk';
import { useForm } from 'react-hook-form';

interface IFriendTimeTableProps {
    users_id: number
    name: string
}

const FriendTimetable: React.FC<IFriendTimeTableProps> = (props) => {
    const dispatch = useDispatch();
    const [friendsDropdown, setFriendsDropdown] = useState(false)
    const [friendName, setFriendName] = useState(props.name)
    const [friendID, setFriendID] = useState<number>(props.users_id)
    const [selectedDate, setSelectedDate] = useState(moment());
    const [myTimetable, setMyTimetable] = useState<any>(null);
    const [inviteForm, setInviteForm] = useState(false)
    const { register, handleSubmit } = useForm();
    const friendAvailableTime = useSelector((state: IRootState) => state.activities.friendAvailableTime)
    const friendsList = useSelector((state: IRootState) => state.friends.friendsList)
    const personalActivities = useSelector((state: IRootState) => state.activities.personalActivities)

    const clickInvite = () => setInviteForm(!inviteForm);
    const inviteFriend = (data: any) => {
        const start_time = moment(data.start_time).utc().format();
        const end_time = moment(data.end_time).utc().format()
        dispatch(inviteFriendThunk(friendID as number, data.description, start_time, end_time, data.deadline, "public", data.location))
        setInviteForm(false)
    }
    const clickMyTimetable = () => {
        setMyTimetable(
            personalActivities.map(personalActivity => (
                <Row className="myTimetable_item">
                    <Col>{moment(personalActivity.start_time).format('HH:mm')} - {moment(personalActivity.end_time).format('HH:mm')}</Col>
                    <Col>{personalActivity.description}</Col>
                    <Col>{personalActivity.location}</Col>
                </Row>
            ))
        )
    }

    const clickFriendsDropdown = () => setFriendsDropdown(!friendsDropdown)

    const closeMyTimetable = () => {
        setMyTimetable(null);
    }

    const clickFriendName = (users_id: number, name: string) => {
        const start_time = moment(selectedDate).format();
        const end_time = moment(selectedDate).format('YYYY-MM-DDT23:59:59Z');
        dispatch(getFriendAvailableTimeThunk(users_id, start_time, end_time));
        setFriendName(name)
        setFriendID(users_id)
    }

    useEffect(() => {
        const start_time = moment(selectedDate).format();
        const end_time = moment(selectedDate).format('YYYY-MM-DDT23:59:59Z');
        dispatch(getFriendAvailableTimeThunk(props.users_id, start_time, end_time));
        dispatch(getPersonalActivityThunk(start_time, end_time))
    }, [dispatch, selectedDate, props.users_id])

    useEffect(() => {
        setMyTimetable(
            personalActivities.map(personalActivity => (
                <Row className="myTimetable_item">
                    <Col>{moment(personalActivity.start_time).format('HH:mm')} - {moment(personalActivity.end_time).format('HH:mm')}</Col>
                    <Col>{personalActivity.description}</Col>
                    <Col>{personalActivity.location}</Col>
                </Row>
            ))
        )
    }, [dispatch, personalActivities])

    return (
        <Container className="friends_list themed-container" fluid={true}>
            <Row>
                <Col className="friendComponent">
                    <Row >
                        <Col xs="12" md="3"><Calendar value={selectedDate} onChange={setSelectedDate} userID={props.users_id} userName={props.name} date={selectedDate} /></Col>
                        <Col md="9">
                            <Row>
                                <Col md="9"></Col>
                                <Col md="2" className="friendTimetableBtn" onClick={clickInvite}>Invite Friend</Col>
                                <Modal isOpen={inviteForm}><ModalBody ><Form onSubmit={handleSubmit(inviteFriend)}>
                                    <Row className="inviteFormHeader"><Col>Invite Friend</Col></Row>
                                    <Row className="inviteRow"><Col className="inputTitle">Description:</Col><Col> <input className="inviteTextInput" type="text" name="description" ref={register} /></Col></Row>
                                    <Row className="inviteRow"><Col className="inputTitle">Location:</Col><Col> <input className="inviteTextInput" type="text" name="location" ref={register} /></Col></Row>
                                    <Row className="inviteRow"><Col className="inputTitle">Start Time : </Col><Col><input type="datetime-local" name="start_time" ref={register} /></Col></Row>
                                    <Row className="inviteRow"><Col className="inputTitle">End Time : </Col><Col><input type="datetime-local" name="end_time" ref={register} /></Col></Row>
                                    <Row className="inviteRow"><Col className="inputTitle">Join Deadline : </Col><Col><input type="datetime-local" name="deadline" ref={register} /></Col></Row>
                                    <Row>
                                        <Col className="inviteSubmitCol"><input className="inviteSubmit" type="submit" value="Submit" /></Col>
                                        <Col className="inviteSubmitCol"><input className="inviteSubmit" value="Cancel" onClick={clickInvite} /></Col>
                                    </Row>
                                </Form></ModalBody></Modal>
                                <Col md="1"></Col>
                            </Row>
                            <Row>
                                <Col md="9"></Col>
                                <Col md="2" className="friendTimetableBtn" onClick={clickMyTimetable}>My Timetable</Col>
                                <Col md="1"></Col>
                            </Row>
                            <Row>
                                <Col md="9"></Col>
                                <Col md="2">
                                    <Dropdown isOpen={friendsDropdown} toggle={clickFriendsDropdown}>
                                        <DropdownToggle className="friendsDropdown" >{friendName}</DropdownToggle>
                                        <DropdownMenu right>
                                            {friendsList && friendsList.map(friend => (
                                                <DropdownItem key={friend.users_id} onClick={() => clickFriendName(friend.users_id, friend.name)}>{friend.name}</DropdownItem>
                                            ))}
                                        </DropdownMenu>
                                    </Dropdown>
                                </Col>
                                <Col md="1"></Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <Row>
                                <Col>
                                    <Row className="friendsTimetable_header"><Col>{friendName}'s Available  Timetable</Col></Row>
                                    <Row className="friendAvailableTimeList"><Col>
                                        {friendAvailableTime.length > 0 && friendAvailableTime.map(times => (
                                            <Row>
                                                <Col className="friendsTimetableItem">{moment(times.start_time).format('HH:mm')} - {moment(times.end_time).format('HH:mm')}</Col>
                                            </Row>))}
                                    </Col></Row>
                                </Col>
                                {myTimetable &&
                                    <Col>
                                        <Row className="friendsTimetable_header">
                                            <Col className="friendsTimetable_Col">My Timetable<span className="closeBtn"><CloseOutlined onClick={closeMyTimetable} /></span></Col>
                                        </Row>
                                        <Row className="friendAvailableTimeList"><Col>
                                            <Row className="myTimetable_header">
                                                <Col>Time</Col>
                                                <Col>Description</Col>
                                                <Col>Location</Col>
                                            </Row>
                                            <Row><Col>{myTimetable}</Col></Row>
                                        </Col></Row>
                                    </Col>
                                }
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>

        </Container>
    )
}

export default FriendTimetable
