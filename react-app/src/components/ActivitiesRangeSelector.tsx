import React from "react"
import moment from 'moment'
import { useForm } from "react-hook-form"
import { Form, Label, Input, Col, Row } from "reactstrap"
import { Button } from '@material-ui/core';
import { useDispatch } from "react-redux";
import { getActivitiesThunk } from "../redux/activities/thunk";

export interface ActivitiesRangeSelector {
    from: string
    to: string
}


const ActivitiesRangeSelector: React.FC = () => {
    // const now = moment().format("YYYY-MM-DD")
    const dispatch = useDispatch();
    // const [now, setNow] = useState(moment().format("YYYY-MM-DD"))
    // const isProcessing = useSelector((state: IRootState) => state.activities.isProcessing)

    const { register, handleSubmit } = useForm<ActivitiesRangeSelector>()
    const onSubmit = (range: ActivitiesRangeSelector) => {
        range.from = moment(range.from).format("YYYY-MM-DDT00:00")
        range.to = moment(range.to).format("YYYY-MM-DDT23:59")
        // console.log(range)
        dispatch(getActivitiesThunk(range))
    }


    return (
        <div>
            <Form className="create-activitive-form" onSubmit={handleSubmit(onSubmit)}>
                <Row>
                    <Col md="1">

                    </Col>
                    <Col md="3">
                        <Row>
                            <Col md="2">
                                <Label for="from">from</Label>
                            </Col>
                            <Col>
                                <Input type="date"
                                    name="from"
                                    placeholder="from"
                                    innerRef={register({
                                        required: "Required"
                                    })} />
                            </Col>
                        </Row>
                    </Col>
                    <Col md="3">
                        <Row>
                            <Col md="2">
                                <Label for="to">to</Label>
                            </Col>
                            <Col>
                                <Input type="date"
                                    name="to"
                                    placeholder="to"
                                    innerRef={register({
                                        required: "Required"
                                    })} />
                            </Col>
                        </Row>
                    </Col>
                    <Col md="5">
                        <Row className="act-selector-btn">
                            <Button type="submit" value="Submit" variant="contained">Search</Button>
                            <Button value="Submit" variant="contained" onClick={() =>
                                dispatch(getActivitiesThunk({ from: "2010-01-01T00:00", to: "2030-12-31T23:59" }))
                            }>Show all</Button>
                        </Row>
                    </Col>

                </Row>





            </Form>
        </div >
    )
}

export default ActivitiesRangeSelector