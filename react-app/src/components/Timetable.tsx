import React, { useState } from 'react'
import { deleteAvailableTimeThunk, editAvailableTimeThunk } from '../redux/activities/thunk'
import { useDispatch, useSelector } from 'react-redux';
import { Container, Row, Col, Modal, ModalBody, Form } from "reactstrap";
import moment from "moment";
import { useForm } from "react-hook-form";
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import '../css/Timetable.css'
import { IRootState } from '../redux/store';

interface IEditTimetableProps {
    date?: any
}

const Timetable: React.FC<IEditTimetableProps> = (props) => {
    const dispatch = useDispatch()
    const { register, handleSubmit } = useForm();
    const [editAvailableForm, setEditAvailableForm] = useState(false)
    const availabletimes = useSelector((state: IRootState) => state.activities.availabletimes);
    const clickEditAvailableForm = () => { setEditAvailableForm(!editAvailableForm) }

    const deleteAvailableTime = (availabletimes_id: number) => {
        dispatch(deleteAvailableTimeThunk(availabletimes_id))
    }

    const editAvailableTime = (data: any) => {
        const availableTimeID = data.availableTimeID
        const date = moment(props.date).format('YYYY-MM-DDT')
        const start_time = date + data.start_time + ":00+08:00"
        const end_time = date + data.end_time + ":00+08:00"
        dispatch(editAvailableTimeThunk({ availableTimeID, start_time, end_time }))
    }

    return (
        <Container className="themed-container" fluid={true}>
            <Row className="personal_timeTableRow">
                <Col className="available_timeTable">
                    {/* <Row><Col className="timeTable-header"><div>Available time</div></Col></Row> */}
                    {availabletimes.length > 0 && availabletimes.map(availabletime => (
                        <Row className="eventItemList" key={availabletime.availabletimes_id}>
                            <Col xs="0" md="4" className="eventItemEmptyCol"></Col>
                            <Col xs="6" md="4" className="eventItem">{moment(availabletime.start_time).format('HH:mm')}-{moment(availabletime.end_time).format('HH:mm')}</Col>
                            <Modal isOpen={editAvailableForm} toggle={clickEditAvailableForm}>
                                <ModalBody>
                                    <Form onSubmit={handleSubmit(editAvailableTime)}>
                                        <Row><Col className="editAvailableTimeFormHeader">Edit Available Time</Col></Row>
                                        <Row className="editAvailableTimeFormRow">
                                            <Col md="2"><input type="hidden" name="availableTimeID" value={availabletime.availabletimes_id} ref={register} /></Col>
                                            <Col>Start time : </Col>
                                            <Col><input type="time" name="start_time" ref={register} /></Col>
                                        </Row>
                                        <Row className="editAvailableTimeFormRow">
                                            <Col md="2"></Col>
                                            <Col>End time : </Col>
                                            <Col><input type="time" name="end_time" ref={register} /></Col>
                                        </Row>
                                        <Row className="editAvailableTimeFormBtnArea">
                                            <Col md="2"></Col>
                                            <Col><input className="editAvailableTimeFormBtn" type="submit" value="Save" onClick={() => setEditAvailableForm(false)} /></Col>
                                            <Col><input className="editAvailableTimeFormBtn" type="button" value="Cancel" onClick={() => setEditAvailableForm(!editAvailableForm)} /></Col>
                                            <Col md="2"></Col>
                                        </Row>
                                    </Form>
                                </ModalBody>
                            </Modal>
                            <Col xs="6" md="2">
                                <EditOutlined className="availableTimeBtn" onClick={clickEditAvailableForm} />
                                <DeleteOutlined className="availableTimeBtn" onClick={() => deleteAvailableTime(availabletime.availabletimes_id)} />
                            </Col>
                            <Col xs="0" md="2" className="eventItemEmptyCol"></Col>
                        </Row>
                    ))}
                </Col>
            </Row>
        </Container>
    )
}

export default Timetable
