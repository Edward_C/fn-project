import React, { useState, useEffect } from 'react';
import { Button, Menu, Dropdown, Spin } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import { putAttendThunk } from '../redux/activities/thunk';
import { IActivity } from '../redux/activities/state';
import { IRootState } from '../redux/store';

interface IProps {
    activity: IActivity
    className?: string
}

const ActDropdownButton: React.FC<IProps> = ({ activity, className }) => {
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState<boolean>(false)
    const activities = useSelector((state: IRootState) => state.activities.activities)

    useEffect(() => {
        setIsLoading(false)
    }, [activities])

    function handleMenuClick(status: any) {
        setIsLoading(true)
        dispatch(putAttendThunk(activity.activities_id, status.key))
    }

    const menu = (
        <Menu onClick={handleMenuClick}>
            <Menu.Item key="attend">ATTEND</Menu.Item>
            <Menu.Item key="refuse">REFUSE</Menu.Item>
        </Menu>
    );

    return (
        <>
            <Dropdown overlay={menu}>
                <Button>
                    {!isLoading && (
                        <>
                            {activity.status.toUpperCase()} <DownOutlined />
                        </>
                    )}
                    {isLoading && <Spin />}
                </Button>
            </Dropdown>
        </>
    )
}

export default ActDropdownButton