import React, { useState } from 'react'
import { Collapse } from 'reactstrap';

const SideBar: React.FC = () => {
    const [sideBarDropdown, setSideBarDropdown] = useState(false)
    const clickSideBarDropdown = () => setSideBarDropdown(!sideBarDropdown)
    return (
        <>
            <div className="sidebarContainer" >
                <div className="sidebar">
                    <div><a href="/">Personal</a></div>
                    <div> <a href="/friends">Friends</a></div>
                    <div><a href="/activities"> Events</a></div>
                </div>
                <div className="sideBarDropdown">
                    <div className="sideBarDropdownBtn" onClick={clickSideBarDropdown}>Pages</div>
                    <Collapse isOpen={sideBarDropdown}>
                        <div><a href="/">Personal</a></div>
                        <div> <a href="/friends">Friends</a></div>
                        <div><a href="/activities"> Events</a></div>
                    </Collapse>
                </div>
            </div>
        </>
    )
}

export default SideBar
