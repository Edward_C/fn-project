import React, { useEffect, useState } from "react"
import { Col, Container, Form, Row } from 'reactstrap';
import { CheckOutlined, CloseOutlined } from '@ant-design/icons';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from "../redux/store"
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Button, Modal, ModalBody } from "reactstrap"
import { logoutThunk, editProfileThunk, restoreLoginThunk } from "../redux/auth/thunk"
import { getRequest, accept, reject } from "../redux/friends/thunks";
import ActivitiesInvitation from "./ActivitiesInvitation";
import '../css/navbar.css'
import { useForm } from "react-hook-form";

const NavBar: React.FC = () => {
    const dispatch = useDispatch();
    const { register, handleSubmit } = useForm();
    const [profile, setProfile] = useState(false);
    const [dropdownOpen, setDropdownOpen] = useState(false);
    const [friendsRequestForm, setFriendsRequestForm] = useState(false);
    const toggle = () => setDropdownOpen(prevState => !prevState);
    const clickFriendsRequest = () => setFriendsRequestForm(!friendsRequestForm);
    const isAuthenticated = useSelector((state: IRootState) => state.auth.isAuthenticated)
    const friendsRequestList = useSelector((state: IRootState) => state.friends.friendsRequestList)
    const isProcessing = useSelector((state: IRootState) => state.friends.isProcessing);
    const editProcessing = useSelector((state: IRootState) => state.auth.editProcessing)
    const user = useSelector((state: IRootState) => state.auth.userInfo);
    const clickProfile = () => setProfile(!profile);
    const acceptRequest = (data: any) => {
        dispatch(accept(data, "accept"));
    }

    const rejectRequest = (data: any) => {
        dispatch(reject(data, "reject"))
    }

    const editProfile = (data: any) => {
        if (data.password === data.confirmPassword) {
            clickProfile()
            dispatch(editProfileThunk(data))
        } else {
            alert("Password does not match")
        }
    }

    useEffect(() => {
        if (!isProcessing && !editProcessing) {
            dispatch(getRequest());
            dispatch(restoreLoginThunk())
        }
    }, [dispatch, isProcessing, editProcessing])

    return (
        <>
            {isAuthenticated && <Container className="navbar themed-container" fluid={true}>
                <Row className="navbarRow">
                    <Col md="8" className='navbar-title'><div className='navbar-title-text'><a id="home" className="menu-item" href="/">EventAssistant</a></div></Col>
                    <Col md="4"><Row>
                        <Col xs="4" md="1">
                            <ActivitiesInvitation />
                        </Col>
                        <Col xs="4" md="1" className="friendsRequestPosition">
                            <Dropdown isOpen={friendsRequestForm} toggle={clickFriendsRequest}>
                                <DropdownToggle className="friendsRequestListLength">{friendsRequestList && friendsRequestList.length}</DropdownToggle>
                                <DropdownMenu className="friendsRequestForm">
                                    {friendsRequestList.length > 0 && friendsRequestList.map(friendsRequest => (
                                        <DropdownItem key={friendsRequest.users_id}>
                                            <Row >
                                                <Col md="8">{friendsRequest.name}</Col>
                                                <Col md="2" onClick={() => acceptRequest(friendsRequest.users_id)}><CheckOutlined /></Col>
                                                <Col md="2" onClick={() => rejectRequest(friendsRequest.users_id)}><CloseOutlined /></Col>
                                            </Row>
                                        </DropdownItem>
                                    ))}
                                    {friendsRequestList.length === 0 && <div className="friendsRequestStatus">No Friends Request</div>}
                                </DropdownMenu>
                            </Dropdown>
                        </Col>
                        <Col xs="4" md="2">
                            {isAuthenticated && user &&
                                <Dropdown isOpen={dropdownOpen} toggle={toggle} className="user-dropdown">
                                    <DropdownToggle caret className="navbar-btn">{user.name}</DropdownToggle>
                                    <DropdownMenu className="navbar-user-menu">
                                        <DropdownItem onClick={clickProfile}><div className="navBtn">Profile</div></DropdownItem>
                                        <Modal isOpen={profile} toggle={clickProfile} >
                                            <ModalBody>
                                                <Row className="profileHeader"><Col><h1>Profile</h1></Col></Row>
                                                <Form onSubmit={handleSubmit(editProfile)}>
                                                    <Row className="profileInputRow">
                                                        <Col md="1"></Col>
                                                        <Col className="profileInputTitle">Display name : </Col>
                                                        <Col className="profileInput"><input type="text" name="name" defaultValue={user.name} ref={register} /></Col>
                                                    </Row>
                                                    <Row className="profileInputRow">
                                                        <Col md="1"></Col>
                                                        <Col className="profileInputTitle">Tel :</Col>
                                                        <Col className="profileInput"><input type="tel" name="tel" defaultValue={user.tel} pattern="[0-9]{8}" ref={register} minLength={8} maxLength={8} /></Col>
                                                    </Row>
                                                    <Row className="profileInputRow">
                                                        <Col md="1"></Col>
                                                        <Col className="profileInputTitle">Password : </Col>
                                                        <Col className="profileInput"><input type="password" name="password" ref={register({ required: "Required" })} minLength={8} /></Col>
                                                    </Row>
                                                    <Row className="profileInputRow">
                                                        <Col md="1"></Col>
                                                        <Col className="profileInputTitle"> Confirm Password : </Col>
                                                        <Col className="profileInput"><input type="password" name="confirmPassword" ref={register({ required: "Required" })} minLength={8} /></Col>
                                                    </Row>
                                                    <Row>
                                                        <Col></Col>
                                                        <Col className="profileBtn"><Button type="submit">Save</Button>{' '}</Col>
                                                        <Col className="profileBtn"><Button onClick={clickProfile}>Cancel</Button></Col>
                                                        <Col></Col>
                                                    </Row>
                                                </Form>
                                            </ModalBody>
                                        </Modal>
                                        <DropdownItem onClick={() => dispatch(logoutThunk())}><div className="navBtn">Logout</div></DropdownItem>
                                    </DropdownMenu>
                                </Dropdown>}
                        </Col>
                    </Row></Col>
                </Row>
            </Container>}
        </>
    )
}

export default NavBar