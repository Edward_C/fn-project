import React, { useEffect, useState } from "react";
import { Container, Row, Col } from "reactstrap";
import AddFriendsForm from '../components/AddFriendsForm';
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../redux/store";
import { friendsListThunk, getRequest } from "../redux/friends/thunks";
import '../css/FriendsPage.css'
import FriendTimetable from "../components/FriendTimetable";



const FriendsPage: React.FC = () => {
    const dispatch = useDispatch();
    const [header, setHeader] = useState<any>(true)
    const [pages, setPages] = useState<any>()
    const friends = useSelector((state: IRootState) => state.friends.friendsList)
    const clickAddFriends = () => { setPages(<AddFriendsForm />) }
    const clickFriends = () => {
        setPages(
            <div>
                {friends && friends.map(friend => (
                    <Row className="friendsNameList">
                        <Col >
                            <Row className="friendsInfo">Name : {friend.name}</Row>
                            <Row className="friendsInfo">Tel : {friend.tel}</Row>
                        </Col>
                        <Col className="inviteFriendCol"><div className="inviteFriend" onClick={() => { setPages(<FriendTimetable users_id={friend.users_id} name={friend.name} />); setHeader(false); }}>Invite</div></Col>
                    </Row>
                ))}
            </div>
        )
    }



    useEffect(() => {
        setPages(
            <div>
                {friends && friends.map(friend => (
                    <Row className="friendsNameList">
                        <Col >
                            <Row className="friendsInfo">Name : {friend.name}</Row>
                            <Row className="friendsInfo">Tel : {friend.tel}</Row>
                        </Col>
                        <Col className="inviteFriendCol"><div className="inviteFriend" onClick={() => { setPages(<FriendTimetable users_id={friend.users_id} name={friend.name} />); setHeader(false); }}>Invite</div></Col>
                    </Row>
                ))}
            </div>
        )
    }, [friends])

    useEffect(() => {
        dispatch(getRequest());
        dispatch(friendsListThunk());
    }, [dispatch])

    return (
        <div>
            <Container className="themed-container" fluid={true}>
                <Row className="friend-empty"></Row>
                <Row className="friendsComponent">
                    <Col>
                        {header && <Row className="friendsComponent-subheader">
                            <Col xs="6" sm="3"><div className="friendsListBtn" onClick={clickFriends}>Friends</div></Col>
                            <Col xs="6" sm="3"><div className="addfriendsBtn" onClick={clickAddFriends}>Add Friends</div></Col>
                        </Row>}
                        <Row>
                            <Col className="friendsPage_body">
                                {pages}
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default FriendsPage