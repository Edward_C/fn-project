import React from "react"
// import { Container } from 'reactstrap';
import "../css/activities.css"
import { Container, Row, Col, Form, FormGroup, Label, Input } from "reactstrap"
import { useForm } from "react-hook-form"
import { useDispatch } from "react-redux"
import { createActivityThunk } from "../redux/activities/thunk"
import { Button } from '@material-ui/core';
import moment from 'moment'
import { useState } from "react"
import useReactRouter from 'use-react-router';

export interface ICreateActivityFormInput {
    description: string
    start_time: string
    end_time: string
    deadline?: string
    location: string
    privacy: string
}

interface ICreateActivitiesPage {
    onClick: () => void
}


export const CreateActivitiesPage: React.FC<ICreateActivitiesPage> = ({ onClick }) => {
    const dispatch = useDispatch()
    const now = moment().format("YYYY-MM-DDTHH:mm")
    // const isProcessing = useSelector((state: IRootState) => state.activities.isProcessing)
    const [endTimeMin, setEndTimeMin] = useState(now);
    const [deadLineMax, setDeadLineMax] = useState("");
    const { register, handleSubmit } = useForm<ICreateActivityFormInput>()
    const onSubmit = (data: ICreateActivityFormInput) => {
        data.privacy = "public"
        // console.log(data)
        dispatch(createActivityThunk(data))
    }


    const { match: { params: { calendar } } } = useReactRouter();

    return (
        <Container className="themed-container" fluid={true}>
            <Form className="create-activitive-form" onSubmit={handleSubmit(onSubmit)}>
                <FormGroup>
                    <Row>
                        <Col className="createEventTitle"><Label for="description">Description : </Label></Col>

                        <Col><Input type="text"
                            className="timeInput"
                            name="description"
                            placeholder="description"
                            innerRef={register({
                                required: "Required"
                            })} /></Col>
                    </Row>
                </FormGroup>
                <FormGroup>
                    <Row>
                        <Col className="createEventTitle"><Label for="start_time">Start time :</Label></Col>
                        <Col><Input type="datetime-local"
                            className="timeInput"
                            name="start_time"
                            placeholder="start time"
                            defaultValue={`${calendar}`}
                            min={`${now}`}
                            onChange={(e) => setEndTimeMin(e.target.value)}
                            innerRef={register({
                                required: "Required"
                            })} /></Col>
                    </Row>
                </FormGroup>
                <FormGroup>
                    <Row>
                        <Col className="createEventTitle"><Label for="end_time">End time :</Label></Col>
                        <Col><Input type="datetime-local"
                            className="timeInput"
                            name="end_time"
                            placeholder="end time"
                            min={`${endTimeMin}`}
                            onChange={(e) => setDeadLineMax(e.target.value)}
                            innerRef={register({
                                required: "Required"
                            })} /></Col>
                    </Row>
                </FormGroup>
                <FormGroup>
                    <Row>
                        <Col className="createEventTitle"><Label for="deadline">Join deadline :</Label></Col>
                        <Col><Input type="datetime-local"
                            className="timeInput"
                            name="deadline"
                            placeholder="deadline"
                            min={`${now}`}//2020-09-18T15:55
                            max={`${deadLineMax}`}
                            innerRef={register({
                                required: "Required"
                            })} /></Col>
                    </Row>
                </FormGroup>
                <FormGroup>
                    <Row>
                        <Col className="createEventTitle"><Label for="location">Location :</Label></Col>
                        <Col><Input type="text"
                            className="timeInput"
                            name="location"
                            placeholder="location"
                            innerRef={register({
                                required: "Required"
                            })} /></Col>
                    </Row>
                </FormGroup>
                {/* {errMessage ?
                    <Alert color="danger">
                        {errMessage}
                    </Alert> : ""
                } */}
                <FormGroup>
                    <Row>
                        <Col ><Button type="submit" value="Submit" variant="contained">Create event</Button></Col>
                        <Col><Button onClick={onClick} variant="contained">Cancel</Button></Col>
                    </Row>
                </FormGroup>
            </Form>
        </Container>
    )
}

export default CreateActivitiesPage


// const now = Date.now()
// const date1 = new Date().getTimezoneOffset()
// console.log(now)
// const date2 = new Date(now);
// const date3 = new Date(now-date1*60*1000);