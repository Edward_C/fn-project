import React from 'react'
import { Col, Container, Row } from 'reactstrap';
import '../css/LoginPage.css'
import { useDispatch, useSelector } from 'react-redux';
import { loginThunk } from '../redux/auth/thunk'
import { IRootState } from '../redux/store'
import { Button } from '@material-ui/core';
import { useForm } from "react-hook-form";

interface IFormInputs {
    username: string;
    password: string;
}

const LoginPage: React.FC = () => {
    const dispatch = useDispatch();
    const isLoginProcessing = useSelector((state: IRootState) => state.auth.isProcessing)
    const errMessage = useSelector((state: IRootState) => state.auth.errMessage)
    const { register, handleSubmit } = useForm<IFormInputs>();
    const onSubmit = (data: IFormInputs) => {
        if (!isLoginProcessing) {
            dispatch(loginThunk(data.username, data.password));
        }
    }
    return (
        <Container>
            <Row className="login-empty" ></Row>
            <Row><Col className="loginForm">
                <Row><Col>
                    <h1 className="header">Sign In</h1>
                    <div>
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className="input">
                                <input
                                    id="username"
                                    name="username"
                                    placeholder="Username"
                                    type="text"
                                    ref={register}
                                />
                            </div>
                            <div className="input">
                                <input
                                    id="password"
                                    name="password"
                                    placeholder="Password"
                                    type="password"
                                    ref={register} />
                            </div>
                            <Row>
                                <Col><div className="signIn-Btn"><Button type="submit" variant="contained">Sign In</Button></div></Col>
                                <Col><div className="signUp-Btn"><Button href="/register" variant="contained">Register</Button></div></Col>
                            </Row>
                        </form>
                        {errMessage &&
                            <Row><Col className="errMessage">{errMessage}</Col></Row>
                        }
                    </div>
                </Col></Row>
            </Col></Row>
        </Container >
    )
}

export default LoginPage
