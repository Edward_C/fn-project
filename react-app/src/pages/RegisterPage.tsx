import React from 'react'
import { Form, FormGroup, Col, Container, Row } from 'reactstrap';
import { Button } from '@material-ui/core';
import { useForm } from "react-hook-form";
import '../css/RegisterPage.css'
import { useDispatch } from 'react-redux';
import { push } from "connected-react-router"

interface IFormInput {
    username: String;
    password: String;
    confirm_password: String;
    name: String;
}



const { REACT_APP_API_SERVER } = process.env;

const RegisterPage: React.FC = () => {
    const dispatch = useDispatch()
    const { register, handleSubmit } = useForm<IFormInput>();
    const registerSubmit = async (data: IFormInput) => {
        if (data.password === data.confirm_password) {
            await fetch(`${REACT_APP_API_SERVER}/user/register`, {
                method: "POST",
                headers: {
                    "content-type": "application/json",
                },
                body: JSON.stringify(data)
            })
            dispatch(push(`/`))
            // console.log(res.status);
        } else if (data.password !== data.confirm_password) {
            alert("Passwords don't match")
        }
        // console.log(data)
    }



    return (
        <Container >
            <Row className="empty"></Row>
            <Row><Col className="registerFormPosition">
                <Form className="registerForm" onSubmit={handleSubmit(registerSubmit)}>
                    <h1 className="registerHeader">Sign Up</h1>
                    <FormGroup>
                        <Col>
                            <input id="username" type="text" placeholder="Username" name="username" ref={register} />
                            <input id="password" type="text" placeholder="Name" name="name" ref={register} />
                        </Col>
                    </FormGroup>
                    <FormGroup>
                        <Col><input className="registerInput" type="tel" placeholder="Tel" name="tel" ref={register} minLength={8} maxLength={8} /></Col>
                    </FormGroup>
                    <FormGroup>
                        <Col><input className="registerInput" type="password" placeholder="Password" name="password" ref={register} minLength={8} /></Col>
                    </FormGroup>
                    <FormGroup>
                        <Col><input className="registerInput" type="password" placeholder="Confirm Password" name="confirm_password" ref={register} /></Col>
                    </FormGroup>
                    <Row className="registerPage-btnArea">
                        <Col></Col>
                        <Col className="submit-btn"><Button type="submit" variant="contained">Submit</Button></Col>
                        <Col><Button href="/login" variant="contained">Sign In</Button></Col>
                        <Col></Col>
                    </Row>
                </Form>
            </Col></Row>
        </Container >
    )
}

export default RegisterPage
