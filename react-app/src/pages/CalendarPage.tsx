import React, { useState } from 'react';
import Calendar from '../components/Calendar'
import { Container, Row, Col, Form } from 'reactstrap';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { Button, Modal, ModalBody } from 'reactstrap';
import { PlusOutlined } from '@ant-design/icons';
import '../css/CalendarPage.css'
import moment from 'moment';
import { createPersonalActivityThunk, getAvailableTimeThunk, getPersonalActivityThunk, setAvailableTimeThunk } from '../redux/activities/thunk';
import { useDispatch, useSelector } from 'react-redux';
import PersonalEvent from '../components/PersonalEvent';
import Timetable from '../components/Timetable';
import { useEffect } from 'react';
import { getRequest } from '../redux/friends/thunks';
import { IRootState } from '../redux/store';
import { useForm } from 'react-hook-form';
import CreateActivitiesPage from './CreateActivitiesPage';

const CalendarPageTest: React.FC = () => {
    const dispatch = useDispatch();
    const [formDropdown, setFormDropdown] = useState(false)
    const { register, handleSubmit } = useForm({ mode: "onBlur" });
    const { register: register2, handleSubmit: handleSubmit2 } = useForm({ mode: "onBlur" });
    const [availableTime, setAvailableTime] = useState(false);
    const [personalForm, setPersonalForm] = useState(false);
    const [activityFrom, setActivityForm] = useState(false);
    const isProcessing = useSelector((state: IRootState) => state.activities.isProcessing)
    const [selectedDate, setSelectedDate] = useState(moment());
    const [components, setComponents] = useState<any>();
    const clickPersonalEvent = (date: any) => {
        setComponents(<PersonalEvent />)
        const start_time = moment(date).format('YYYY-MM-DDT00:00:00Z');
        const end_time = moment(date).format('YYYY-MM-DDT23:59:59Z');
        dispatch(getPersonalActivityThunk(start_time, end_time))
    }

    const clickAvailableTime = (date: any) => {
        setComponents(<Timetable date={selectedDate} />);
        const start_time = moment(date).format('YYYY-MM-DDT00:00:00Z');
        const end_time = moment(date).format('YYYY-MM-DDT23:59:59Z');
        dispatch(getAvailableTimeThunk({ start_time, end_time }))
    }

    const clickAvailableTimeForm = () => {
        setAvailableTime(!availableTime)
    }

    const clickPersonalForm = () => {
        setPersonalForm(!personalForm)
    };

    const clickActivityForm = () => {
        setActivityForm(!activityFrom)
    }

    const clickFromDropdown = () => setFormDropdown(!formDropdown)

    const cancelForm = () => {
        setAvailableTime(false)
        setPersonalForm(false)
        setActivityForm(false)
    }

    const submitAvailableTime = (data: any) => {
        dispatch(setAvailableTimeThunk(data));
        cancelForm()
    }

    const onSubmit = (data: any) => {
        dispatch(createPersonalActivityThunk(data))
        cancelForm()
    }

    useEffect(() => {
        if (!isProcessing) {
            const date = selectedDate;
            // const start_timePart1 = moment(date).format().substr(0, 10);
            // const start_time = moment(`${start_timePart1} 00:00:00+08`).utc().format()
            // const end_time = moment(`${start_timePart1} 23:59:59+08`).utc().format()
            const start_time = moment(date).format('YYYY-MM-DDT00:00:00Z');
            const end_time = moment(date).format('YYYY-MM-DDT23:59:59Z');
            dispatch(getPersonalActivityThunk(start_time, end_time))
            dispatch(getAvailableTimeThunk({ start_time, end_time }))
        }
    }, [dispatch, isProcessing, selectedDate])

    useEffect(() => {
        dispatch(getRequest());
        setComponents(<PersonalEvent />)
        const start_time = moment().format('YYYY-MM-DDT00:00:00Z');
        const end_time = moment().format('YYYY-MM-DDT23:59:59Z');
        dispatch(getPersonalActivityThunk(start_time, end_time));
    }, [dispatch])

    return (
        <Container className="themed-container" fluid={true}>
            <Row className="personalPageRow">
                <Col>
                    <Row className="personalHeaderRow">
                        <Col md="9">
                            <Row>
                                <Col className="personalHeader">Personal</Col>
                            </Row>
                            <Row className="personalSubheaderRow">
                                <Col className="personalSubheaderCol">
                                    <span className="personalSubheaderBtn" onClick={clickPersonalEvent}>Personal Event</span>
                                    <span className="personalSubheaderBtn" onClick={clickAvailableTime}>Available Time</span>
                                </Col>
                            </Row>
                        </Col>
                        <Col md="3" className="personalPageCalendar">
                            <Calendar value={selectedDate} onChange={setSelectedDate} />
                        </Col>
                    </Row>
                    <Row className="personalPageFormRow">
                        <Col md="9"></Col>
                        <Col md="3" className="personalPageFormCol">
                            <Dropdown isOpen={formDropdown} toggle={clickFromDropdown}>
                                <DropdownToggle className="dropdownToggle"><PlusOutlined className="dropdownToggleIcon" /></DropdownToggle>
                                <DropdownMenu right>
                                    <DropdownItem onClick={clickAvailableTimeForm}>Set available time</DropdownItem>
                                    <DropdownItem onClick={clickPersonalForm}>Create Personal Event</DropdownItem>
                                    <DropdownItem onClick={clickActivityForm}>Create event</DropdownItem>
                                </DropdownMenu>
                            </Dropdown>
                        </Col>
                    </Row>
                    <Row className="personalComponents">
                        <Col className="personalComponentsCol">
                            {components}
                        </Col>
                    </Row>
                </Col>
                <Modal className="availableTimeForm" isOpen={availableTime} toggle={cancelForm}>
                    <ModalBody >
                        <Form onSubmit={handleSubmit(submitAvailableTime)}>
                            <Row><Col className="availableTimeFormHeader">Available Time</Col></Row>
                            <Row><Col ><span className="InputTitle">Start Time :</span><input type="datetime-local" className="startTimeInput" name="start_time" ref={register({ required: "Required" })} /></Col></Row>
                            <Row><Col ><span className="InputTitle">End Time :</span><input type="datetime-local" className="endTimeInput" name="end_time" ref={register({ required: "Required" })} /></Col></Row>
                            <Row>
                                <Col className="submitBtn"><Button type="submit" value="Submit" variant="contained">Submit</Button></Col>
                                <Col><Button onClick={cancelForm}>Cancel</Button></Col>
                            </Row>
                        </Form>
                    </ModalBody>
                </Modal>
                <Modal isOpen={personalForm} toggle={cancelForm}>
                    <ModalBody>
                        <Form className="timeTable-form" onSubmit={handleSubmit2(onSubmit)}>
                            <Row><Col className="timeTableFormHeader">Personal Event</Col></Row>
                            <Row><Col><div className="InputTitle" >Description :</div> <input type="text" name="description" className="descriptionInput" ref={register2({ required: "Required" })} /></Col></Row>
                            <Row><Col><div className="InputTitle" >Location : </div><input type="text" name="location" className="locationInput" ref={register2({ required: "Required" })} /></Col></Row>
                            <Row><Col><div className="InputTitle" >Start Time : </div><input type="datetime-local" className="startTimeInput" name="start_time" ref={register2({ required: "Required" })} /></Col></Row>
                            <Row><Col><div className="InputTitle" >End Time : </div><input type="datetime-local" className="endTimeInput" name="end_time" ref={register2({ required: "Required" })} /></Col></Row>
                            <Row>
                                <Col className="submitBtn"><Button type="submit" value="Submit" variant="contained">Submit</Button></Col>
                                <Col><Button onClick={cancelForm}>Cancel</Button></Col>
                            </Row>
                        </Form>
                    </ModalBody>
                </Modal>
                <Modal isOpen={activityFrom} toggle={cancelForm}>
                    <ModalBody>
                        <CreateActivitiesPage onClick={cancelForm} />
                    </ModalBody>
                </Modal>
            </Row>
        </Container>
    )
}

export default CalendarPageTest
