import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { inviteUserThunk, getActivitiesThunk, getAttendsThunk } from '../redux/activities/thunk';
import { IRootState } from '../redux/store';
import '../css/activities.css';
import { Col, Row, Container } from 'reactstrap';
import { Button } from '@material-ui/core';
import { push } from 'connected-react-router';
import useReactRouter from 'use-react-router';
import { IActivity } from '../redux/activities/state';
import Activity from '../components/Activity';
// import moment from 'moment'
import ActivitiesRangeSelector from '../components/ActivitiesRangeSelector';
import ActDropdownButton from '../components/ActDropdownButton';
import { CheckCircleTwoTone, QuestionCircleTwoTone, CloseCircleTwoTone, PlusCircleTwoTone } from '@ant-design/icons';
import ActManageBtn from '../components/ActManageBtn';
// import { setActivitiesRange } from '../redux/activities/action';

const ActivitiesPage: React.FC = () => {
    const dispatch = useDispatch();
    const [activity, setActivity] = useState<null | IActivity>(null);
    const activities = useSelector((state: IRootState) => state.activities.activities)
    const isProcessing = useSelector((state: IRootState) => state.activities.isProcessing)
    const friendsInvited = useSelector((state: IRootState) => state.activities.friendsInvited)
    const friendsNotInvited = useSelector((state: IRootState) => state.activities.friendsNotInvited)
    // const activitiesRange = useSelector((state: IRootState) => state.activities.activitiesRange)

    const { match: { params: { id } } } = useReactRouter();

    const inviteUser = (friendUserID: number, activityID: number) => {
        dispatch(inviteUserThunk(friendUserID, activityID))
    }


    useEffect(() => {
        const range = { from: "2010-01-01T00:00", to: "2030-12-31T23:59" }
        // range.from = moment().format("YYYY-MM-DDT00:00")
        // range.to = moment(new Date()
        //     .setFullYear(new Date().getFullYear() + 10))
        //     .format("YYYY-MM-DDT23:59")
        dispatch(getActivitiesThunk(range))

    }, [dispatch])

    useEffect(() => {
        if (!isProcessing) {
            // dispatch(getActivitiesThunk(activitiesRange))
        }
    }, [dispatch, isProcessing])

    useEffect(() => {
        if (id) {
            // dispatch(setActivitiesRange({ from: "2010-01-01T00:00", to: "2030-12-31T23:59" }))
            dispatch(getAttendsThunk(id))
            const [filteredActivity] = activities.filter(activity => activity.activities_id === parseInt(id))
            setActivity(filteredActivity)
        }
    }, [activities, id, dispatch])


    return (
        <>
            <Container >
                <h1 className="activitiesPageHeader">Events</h1>
                {!id && <ActivitiesRangeSelector />}
                {activities && activities.length > 0 &&
                    <div className="activities-page">
                        {!id && activities.map((activity, idx) => (
                            <Activity
                                key={`${idx}`}
                                activity={activity}
                                button={<>
                                    {activity.role === "initiator" && <ActManageBtn activity={activity} />}
                                    <ActDropdownButton activity={activity} />
                                    <div><Button variant="contained"
                                        onClick={() => dispatch(push(`/activities/${activity.activities_id}`))}>
                                        MORE</Button></div>
                                </>} />
                        ))}
                    </div>
                }
                {
                    activity && id && <>
                        <Activity
                            activity={activity}
                            button={<>
                                {activity.role === "initiator" && <ActManageBtn activity={activity} />}
                                <ActDropdownButton activity={activity} />
                            </>}>
                            <div className="act-border"></div >
                            {activity.role === "initiator" && <>
                                {friendsNotInvited && friendsNotInvited.map((friend, idx) => (
                                    <Row key={`${idx}`}>
                                        <Col><img src={`https://api.adorable.io/avatars/25/${friend.friendName}.png`} alt="" />
                                            <span>&nbsp;&nbsp;&nbsp;</span>{friend.friendName}
                                        </Col>
                                        <Col>
                                            <Button variant="contained" onClick={() => inviteUser(friend.friendUserID, activity.activities_id)}>
                                                <PlusCircleTwoTone className="act-icon" />
                                                <span>&nbsp;&nbsp;&nbsp;</span>
                                                INVITE
                                                </Button>
                                        </Col>
                                    </Row>
                                ))}
                            </>}
                            {friendsInvited && friendsInvited.map((friend, idx) => (
                                <Row key={`${idx}`}>
                                    <Col><img src={`https://api.adorable.io/avatars/25/${friend.friendName}.png`} alt="" />
                                        <span>&nbsp;&nbsp;&nbsp;</span>{friend.friendName}
                                    </Col>
                                    <Col>
                                        <Button variant="contained">
                                            {friend.status === "attend" && <CheckCircleTwoTone className="act-icon" twoToneColor="#52c41a" />}
                                            {friend.status === "pending" && <QuestionCircleTwoTone className="act-icon" twoToneColor="#b5b5b5" />}
                                            {friend.status === "refuse" && <CloseCircleTwoTone className="act-icon" twoToneColor="#c41a1a" />}
                                            <span>&nbsp;&nbsp;&nbsp;</span>
                                            {friend.status}
                                        </Button>
                                    </Col>
                                </Row>
                            ))}
                            <Row id="act-back-btn">
                                <Button variant="contained"
                                    onClick={() => dispatch(push(`/activities`))}>
                                    BACK</Button>
                            </Row>

                        </Activity>
                    </>
                }
                {
                    id && !activity && activities.length > 0 &&
                    <Row className="activities" >
                        Creating activity failed or the activity is not exist.
                </Row>
                }
            </Container>
        </>
    )
}

export default ActivitiesPage