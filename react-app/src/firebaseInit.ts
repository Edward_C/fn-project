import { notification } from 'antd';
import firebase from 'firebase/app';
import 'firebase/messaging';

interface IPayload {
    data: {
        title: string,
        body: string,
        link: string
    }
}

const config = {
    apiKey: "AIzaSyCH4IEGbmb72UkhTxY7Y8aRnjss3_4Oup4",
    authDomain: "tecky-fn-project.firebaseapp.com",
    databaseURL: "https://tecky-fn-project.firebaseio.com",
    projectId: "tecky-fn-project",
    storageBucket: "tecky-fn-project.appspot.com",
    messagingSenderId: "766331322897",
    appId: "1:766331322897:web:56ba9f1d0c3d4c6610a7b0",
    measurementId: "G-54KMFZWCTR"
};

firebase.initializeApp(config);
const messaging = firebase.messaging();

export const requestFirebaseNotificationPermission = () =>
    new Promise((resolve, reject) => {
        messaging
            .requestPermission()
            .then(() => messaging.getToken())
            .then((firebaseToken) => {
                resolve(firebaseToken);
            })
            .catch((err) => {
                reject(err);
            });
    });

export const onMessageListener = () =>
    new Promise((resolve) => {
        messaging.onMessage((payload: IPayload) => {
            notification.open({
                message: payload.data.title,
                description:
                    `${payload.data.body}`,
                onClick: () => {
                    window.location.href = payload.data.link
                },
            })
            resolve(payload);
        });
    });