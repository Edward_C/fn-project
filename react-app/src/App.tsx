import React, { useEffect } from 'react';
import "./App.css"
import { Col, Container, Row } from 'reactstrap';
import './css/sidebar.css';
import { Switch, Route } from 'react-router-dom';
import PrivateRoute from './components/PrivateRoutes'
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from './redux/store';
import { putUserbyFirebaseTokenThunk, restoreLoginThunk } from './redux/auth/thunk'
import LoginPage from './pages/LoginPage';
import SideBar from './components/SideBar';
import CreateActivitiesPage from './pages/CreateActivitiesPage';
import RegisterPage from './pages/RegisterPage';
import FriendsPage from './pages/FriendsPage';
import ActivitiesPage from './pages/ActivitiesPage';
import CalendarPage from './pages/CalendarPage'
import NavBar from './components/NavBar';
import 'antd/dist/antd.css'
import { onMessageListener } from './firebaseInit';
import { getActivitiesThunk } from './redux/activities/thunk';



function App() {
  const dispatch = useDispatch();
  const isAuthenticated = useSelector((state: IRootState) => state.auth.isAuthenticated)
  const user = useSelector((state: IRootState) => state.auth.userInfo);
  useEffect(() => {
    if (isAuthenticated === null) {
      dispatch(restoreLoginThunk());
    }
    if (isAuthenticated === true && !user) {
      dispatch(restoreLoginThunk());
    }
  }, [dispatch, isAuthenticated, user])

  useEffect(() => {
    if (isAuthenticated === true) {
      dispatch(putUserbyFirebaseTokenThunk());
    }
  }, [dispatch, isAuthenticated])

  onMessageListener()
    .then((payload) => {
      dispatch(getActivitiesThunk({ from: "2010-01-01T00:00", to: "2030-12-31T23:59" }))
    })
    .catch((err) => {
      console.info(`onMessageListener: ${err}`)
    });



  return (
    <div className="">
      <Container className="themed-container app-container" fluid={true}>
        {isAuthenticated !== null && (
          <Row>
            {isAuthenticated && <Col md="2" ><SideBar /></Col>}
            <Col>
              <Row>
                <Col><NavBar /></Col>
              </Row>
              <Row>
                <Col>
                  <Switch>
                    <Route path="/login" exact={true} component={LoginPage} />
                    <Route path="/register" exact={true} component={RegisterPage} />
                    <PrivateRoute path="/create_activities" exact={true} component={CreateActivitiesPage} />
                    <PrivateRoute path="/create_activities/:calendar" exact={true} component={CreateActivitiesPage} />
                    <PrivateRoute path="/activities" exact={true} component={ActivitiesPage} />
                    <PrivateRoute path="/activities/:id" exact={true} component={ActivitiesPage} />
                    <PrivateRoute path="/friends" exact={true} component={FriendsPage} />
                    <PrivateRoute path="/" exact={true} component={CalendarPage} />
                  </Switch>
                </Col>
              </Row>
            </Col>
          </Row>
        )}
      </Container>
    </div>
  );
}

export default App;
